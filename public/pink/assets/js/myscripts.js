jQuery(document).ready(function () {

    jQuery('.commentlist li').each(function (i) {
        jQuery(this).find('div.commentNumber').text('# '+i);
    });

    $('.comment-reply-link').on('click',function(){

     var p_id= $(this).next().html();
     $('#comment_parent').val(p_id);
    });

    jQuery('#commentform').on('click','#submit',function (e) {
        e.preventDefault();


        var comParent = jQuery(this);
    jQuery('.wrap_result').css('display','block').text('Kommentariy yatda saklanylyar...').fadeIn(500, function(){
        var data = jQuery('#commentform').serializeArray();
        //alert(data);
        //console.log(data);
        //console.log($data);
        jQuery.ajax({
            url:jQuery('#commentform').attr('action'),
            data:data,
            //headers:{'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content'),
            type:'POST',
            datatype:'JSON',
            success:function(html){
                if (html.error){
                    $('.wrap_result').css('color','red').css('border','1px solid red')
                        .html("<strong>Yalnyshlyk yuze chykdy:<br/></strong>"+html.error.join("<br/>"));
                } else
                    if(html.success){
                       $('.wrap_result').css('color','green').css('border','1px solid green').
                       html("<strong>Yatda saklanyldy!</strong>")
                           .delay(1500).fadeOut(500,function(){
                           $('#commentform').prev().before(html.view_comment);
                       });
                }
            },
            error:function (err) {
                $('.wrap_result').css('color','red').css('border','1px solid red')
                    .html("<strong>Bagyshlan!Serverde yalnyshlyk yuze chykdy, tazeden synansyn!<br/></strong>");
            }
        })
    });
    })


})

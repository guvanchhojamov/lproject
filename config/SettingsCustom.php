<?php
  return  [
  'slider_path'=>'slider-cycle',
  'bar_side_on_index'=>'right',//right,left,no
  'bar_side_on_articles'=>'right', //right,left,no
  'count_portfolio_items' => 5,
  'count_article_items'=>3,
  'articles_per_page' => 2,
  'count_comment_right_bar'=>3,
  'count_portfolio_right_bar'=>3,
  'portfolios_per_page'=>4,
  'portfolios_count_on_detail_page'=>7,
  'contacts_bar_side'=>'left',
  'mail_admin'=>'admin@admin.ru',
  'auth_redirect_url'=>'/admin',
  'auth_field_for_login'=>'login', // or email...
  'admin_prefix'=>'admin',

  'articles_img'=>[
          'max'=>['width'=>816,'height'=>282],
          'mini'=>['width'=>55,'height'=>55]
      ],
  'image'=>['width'=>1024,'height'=>400],

  ];
?>
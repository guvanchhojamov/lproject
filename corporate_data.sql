-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 25 2020 г., 18:37
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `corporate_data`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `category_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `full_text`, `alias`, `image`, `created_at`, `updated_at`, `user_id`, `category_id`, `description`, `keywords`, `meta_desc`) VALUES
(1, 'Section shortcodes & sticky posts!', '<p>Fusce nec accumsan eros. Aenean ac orci a magna vestibulum posuere quis nec nisi. Maecenas rutrum vehicula condimentum. Donec volutpat nisl ac mauris consectetur gravida.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel vulputate nibh. Pellentesque habitant <strong>morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas.</p>\r\n\r\n<p>In facilisis ornare arcu, sodales facilisis neque blandit ac. Ut blandit ipsum quis arcu adipiscing <strong>sit amet semper</strong> sem feugiat. Nam sed dapibus arcu. Nullam eleifend molestie lectus. Nullam nec risus purus.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Fusce rutrum lectus id nibh ullamcorper aliquet. Pellentesque pretium mauris et augue fringilla non bibendum turpis iaculis. Donec sit amet nunc lorem. Sed fringilla vehicula est at pellentesque. Aenean imperdiet elementum arcu id facilisis. Mauris sed leo eros.</p>\r\n\r\n<p>Duis nulla purus, malesuada in gravida sed, viverra at elit. Praesent nec purus sem, non imperdiet quam. Praesent tincidunt tortor eu libero scelerisque quis consequat justo elementum. Maecenas aliquet facilisis ipsum, commodo eleifend odio ultrices et. Maecenas arcu arcu, luctus a laoreet et, fermentum vel lectus. Cras consectetur ipsum venenatis ligula aliquam hendrerit. Suspendisse rhoncus hendrerit fermentum. Ut eget rhoncus purus.</p>\r\n\r\n<p>Cras a tellus eu justo lobortis tristique et nec mauris. Etiam tincidunt tellus ut odio elementum adipiscing. Maecenas cursus dolor sit amet leo elementum ut semper velit lobortis. Pellentesque posue</p>', 'article-1', '{\"mini\":\"0037-55x55.jpg\",\"max\":\"003-816x282.jpg\",\"first\":\"0081-385x192.jpg\",\"path\":\"001-816x282.jpg\"}', '2020-03-03 07:23:46', '2020-04-09 05:05:44', 2, 1, '<p>Fusce nec accumsan eros. Aenean ac orci a magna vestibulum posuere quis nec nisi. Maecenas rutrum vehicula condimentum. Donec volutpat nisl ac mauris consectetur gravida.</p>', 'Section shortcodes & sticky posts', '<p>Fusce nec accumsan eros. Aenean ac orci a magna vestibulum posuere quis nec nisi. Maecenas rutrum vehicula condimentum. Donec volutpat nisl ac mauris consectetur gravida.</p>'),
(2, 'This is the title of the first article. Enjoy it.', '<p>Fusce nec accumsan eros. Aenean ac orci a magna vestibulum posuere quis nec nisi. Maecenas rutrum vehicula condimentum. Donec volutpat nisl ac mauris consectetur gravida.</p>\r\n				                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel vulputate nibh. Pellentesque habitant <strong>morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas.</p>\r\n				                    <p>In facilisis ornare arcu, sodales facilisis neque blandit ac. Ut blandit ipsum quis arcu adipiscing <strong>sit amet semper</strong> sem feugiat. Nam sed dapibus arcu. Nullam eleifend molestie lectus. Nullam nec risus purus.</p>\r\n				                    <p><span id=\"more-41\"></span></p>\r\n				                    <p>Fusce rutrum lectus id nibh ullamcorper aliquet. Pellentesque pretium mauris et augue fringilla non bibendum turpis iaculis. Donec sit amet nunc lorem. Sed fringilla vehicula est at pellentesque. Aenean imperdiet elementum arcu id facilisis. Mauris sed leo eros.</p>\r\n				                    <p>Duis nulla purus, malesuada in gravida sed, viverra at elit. Praesent nec purus sem, non imperdiet quam. Praesent tincidunt tortor eu libero scelerisque quis consequat justo elementum. Maecenas aliquet facilisis ipsum, commodo eleifend odio ultrices et. Maecenas arcu arcu, luctus a laoreet et, fermentum vel lectus. Cras consectetur ipsum venenatis ligula aliquam hendrerit. Suspendisse rhoncus hendrerit fermentum. Ut eget rhoncus purus.</p>\r\n				                    <p>Cras a tellus eu justo lobortis tristique et nec mauris. Etiam tincidunt tellus ut odio elementum adipiscing. Maecenas cursus dolor sit amet leo elementum ut semper velit lobortis. Pellentesque posue</p>', 'article-2', '{\"mini\":\"003-55x55.jpg\",\"max\":\"003-816x282.jpg\",\"first\":\"0081-385x192.jpg\",\"path\":\"003-816x282.jpg\"}\r\n', '2020-03-01 19:41:00', NULL, 2, 2, '<p>Fusce nec accumsan eros. Aenean ac orci a magna vestibulum posuere quis nec nisi. Maecenas rutrum vehicula condimentum. Donec volutpat nisl ac mauris consectetur gravida.</p>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel vulputate nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>', 'This is the title of the first article. Enjoy it.', 'This is a full width page'),
(3, 'This is a full width page', '<p>In facilisis ornare arcu, sodales facilisis neque blandit ac. Ut blandit ipsum quis arcu adipiscing <strong>sit amet semper</strong> sem feugiat. Nam sed dapibus arcu. Nullam eleifend molestie lectus. Nullam nec risus purus.</p>\r\n				                    <p><span id=\"more-41\"></span></p>\r\n				                    <p>Fusce rutrum lectus id nibh ullamcorper aliquet. Pellentesque pretium mauris et augue fringilla non bibendum turpis iaculis. Donec sit amet nunc lorem. Sed fringilla vehicula est at pellentesque. Aenean imperdiet elementum arcu id facilisis. Mauris sed leo eros.</p>\r\n				                    <p>Duis nulla purus, malesuada in gravida sed, viverra at elit. Praesent nec purus sem, non imperdiet quam. Praesent tincidunt tortor eu libero scelerisque quis consequat justo elementum. Maecenas aliquet facilisis ipsum, commodo eleifend odio ultrices et. Maecenas arcu arcu, luctus a laoreet et, fermentum vel lectus. Cras consectetur ipsum venenatis ligula aliquam hendrerit. Suspendisse rhoncus hendrerit fermentum. Ut eget rhoncus purus.</p>\r\n				                    <p>Cras a tellus eu justo lobortis tristique et nec mauris. Etiam tincidunt tellus ut odio elementum adipiscing. Maecenas cursus dolor sit amet leo elementum ut semper velit lobortis. Pellentesque posue</p>', 'article-3', '{\"mini\":\"001-55x55.png\",\"max\":\"001-816x282.jpg\",\"first\":\"0081-385x192.jpg\",\"path\":\"00212-816x282.jpg\"}\r\n', '2020-03-04 08:58:14', NULL, 2, 3, '<p>This is an example page. It’s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes).</p>', 'This is a full width page', 'This is a full width page'),
(7, 'Prosto text title taze goshulan materiyal342%$#', '<p>dadadadadadd</p>', 'prosto-text-title-taze-goshulan-materiyal342', '{\"mini\":\"KRTAA4F4_mini.jpg\",\"max\":\"KRTAA4F4_max.jpg\",\"path\":\"KRTAA4F4.jpg\"}', '2020-04-07 07:34:50', '2020-04-09 04:56:46', 2, 4, '<p>asdadad</p>', 'dasdasdadas', 'ads'),
(8, 'Taze materiyal barlag uchin#$$%#%#qewee', '<p>dadadada</p>', 'taze-materiyal-barlag-uchin-qewee', '{\"mini\":\"IwPPQUBP_mini.jpg\",\"max\":\"IwPPQUBP_max.jpg\",\"path\":\"IwPPQUBP.jpg\"}', '2020-04-07 07:41:49', '2020-04-08 15:20:17', 2, 4, '<p>dsada</p>', NULL, 'dasads');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Blog', 0, 'blog', '2020-03-01 19:00:00', NULL),
(2, 'Kompyutery', 1, 'computers', '2020-03-03 05:00:24', NULL),
(3, 'Interesnye', 1, 'interesting', '2020-03-03 01:45:18', NULL),
(4, 'Sovety', 1, 'sovety', '2020-03-03 14:40:16', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `article_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `name`, `email`, `site`, `text`, `parent_id`, `created_at`, `updated_at`, `article_id`, `user_id`) VALUES
(1, 'Test', 'test@mail.ru', 'http://site.tm', 'cHello world comment.', 0, '2020-03-09 19:00:00', NULL, 1, NULL),
(2, 'Hello name!', 'test@mail.ru', 'http://site.tm\r\n', 'cHello world comment.', 0, '2020-03-03 01:45:18', NULL, 1, 2),
(3, '', '', '', 'comment text 1', 1, '2020-03-09 19:00:00', NULL, 1, 2),
(4, 'Salam', 'salam@mail.ru', 'salam.com', 'Salam comment text', 3, '2020-03-02 19:32:19', NULL, 1, 2),
(5, 'Comment5', 'comment@mail.ru', 'comment.ru', 'This comment text ', 5, '2020-03-03 14:40:16', NULL, 1, NULL),
(6, 'Text name', 'nametext@mail.ru', 'textsite.ru', 'comment text', 1, '2020-03-09 19:00:00', NULL, 1, 2),
(7, 'dadsaa', 'dsasdadsa', 'sdadsadas', 'sdadas', 0, '2020-03-21 08:03:54', '2020-03-21 08:03:54', 2, NULL),
(8, 'Guvanch', 'guvanchhojamov@gmail.com', 'sad', 'sadsdadsa', 3, '2020-03-21 08:04:45', '2020-03-21 08:04:45', 1, NULL),
(9, 'Guvanch', 'test2@mail.ru', 'sad', 'cadasa', 8, '2020-03-21 08:14:31', '2020-03-21 08:14:31', 1, NULL),
(10, 'efsdf', 'dfs', 'sdfdsf', 'dsfdfs', 4, '2020-03-21 12:04:39', '2020-03-21 12:04:39', 1, NULL),
(11, 'dsa', 'asd', 'das', 'asd', 0, '2020-03-22 06:57:15', '2020-03-22 06:57:15', 1, NULL),
(12, 'dsa', 'das', 'das', 'das', 0, '2020-03-22 06:58:30', '2020-03-22 06:58:30', 1, NULL),
(13, 'dsa', 'dsa', 'das', 'dasasd', 0, '2020-03-22 07:00:31', '2020-03-22 07:00:31', 1, NULL),
(14, 'dsasda', 'asddasasd', 'asdasddas', 'sdaasdasds', 0, '2020-03-22 07:15:40', '2020-03-22 07:15:40', 1, NULL),
(15, 'dsasadasd', 'dassda', 'dasasd', 'asdasdasd', 4, '2020-03-22 07:18:11', '2020-03-22 07:18:11', 1, NULL),
(16, 'TEst', 'test2@mail.ru', 'sad', 'TEste comnment', 14, '2020-03-22 07:33:38', '2020-03-22 07:33:38', 1, NULL),
(17, 'dsadsa', 'dsadasasd', 'das', 'dsasdaasdasdasddas', 16, '2020-03-22 07:34:07', '2020-03-22 07:34:07', 1, NULL),
(18, 'Java', 'demo@gmail.com', 'ds', 'sfddfsdsfsdfsdfsdfsdffsddfssdf', 0, '2020-03-22 07:34:35', '2020-03-22 07:34:35', 1, NULL),
(19, '31232131313213', 'daadsdasdas231132131', '321321', '231123321132', 6, '2020-03-22 07:35:41', '2020-03-22 07:35:41', 1, NULL),
(20, 'Guvanch', 'adssadsad', 'sadasdsda', 'asddasdsadsaasdasd', 0, '2020-03-22 07:39:57', '2020-03-22 07:39:57', 1, NULL),
(21, '456465', 'asddas', 'dasdsa', 'dsasda', 7, '2020-03-22 07:41:14', '2020-03-22 07:41:14', 2, NULL),
(22, 'Ballak', 'admin@mail.ru', 'sad', '456476s5adasd', 0, '2020-03-22 07:41:31', '2020-03-22 07:41:31', 2, NULL),
(23, 'Test User 2', '789dsa@test.ru', 'dsa', 'dsadasads', 0, '2020-03-22 07:42:05', '2020-03-22 07:42:05', 3, NULL),
(24, 'Java', 'demo@gmail.com', 'sad', 'sdaasdasd', 23, '2020-03-22 07:42:24', '2020-03-22 07:42:24', 3, NULL),
(25, 'Jogap', 'jogap', 'jogap.ru', 'jogap commentariy', 12, '2020-03-22 07:44:51', '2020-03-22 07:44:51', 1, NULL),
(26, 'sdadas', 'dsda@mail.ru', 'ssda', 'kopkpcxxc', 0, '2020-03-22 14:38:53', '2020-03-22 14:38:53', 3, NULL),
(27, 'Ballak', 'adsasdas', 'dsa', 'bhjhb', 0, '2020-03-23 06:34:20', '2020-03-23 06:34:20', 3, NULL),
(28, 'Test', 'test2@mail.ru', 'dsa', 'Wertolet', 26, '2020-03-23 06:35:03', '2020-03-23 06:35:03', 3, NULL),
(29, 'Guvanch', 'guvanchhojamov@gmail.com', NULL, 'Taze duzedilenden sonky commentariya', 0, '2020-04-09 06:26:10', '2020-04-09 06:26:10', 8, 2),
(30, 'Guvanch', 'guvanchhojamov@gmail.com', NULL, 'Jogap commentariy', 29, '2020-04-09 06:26:48', '2020-04-09 06:26:48', 8, 2),
(33, 'Guvanch', 'guvanchhojamov@gmail.com', NULL, 'dsfsdfsd', 0, '2020-04-12 04:57:58', '2020-04-12 04:57:58', 3, 2),
(34, 'Guvanch', 'guvanchhojamov@gmail.com', NULL, '49849849', 27, '2020-04-12 04:58:46', '2020-04-12 04:58:46', 3, 2),
(35, 'Guvanch', 'guvanchhojamov@gmail.com', NULL, 'dsadasda test', 0, '2020-06-23 10:12:54', '2020-06-23 10:12:54', 3, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `filters`
--

CREATE TABLE `filters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `filters`
--

INSERT INTO `filters` (`id`, `title`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Brand Identity', 'brand-identity', '2020-03-03 07:23:46', NULL),
(2, 'Web desing', 'web-design', '2020-03-03 01:45:18', NULL),
(3, 'Web development', 'web-development', '2020-03-09 19:00:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `title`, `url_link`, `parent`, `created_at`, `updated_at`) VALUES
(1, 'Esasy', 'http://corporate.local', '0', '2020-02-29 19:00:00', NULL),
(2, 'Blog', 'http://corporate.local/articles', '0', '2020-03-02 19:32:19', NULL),
(3, 'Kompyutery', 'http://corporate.local/articles/cat/computers', '2', '2020-03-03 07:23:46', NULL),
(4, 'Intweresnye', 'http://corporate.local/articles/cat/interesting', '2', '2020-03-03 01:45:18', NULL),
(5, 'Sovety', 'http://corporate.local/articles/cat/sovety', '2', '2020-03-01 19:41:00', NULL),
(6, 'Portfolios', 'http://corporate.local/portfolios', '0', '2020-03-01 19:41:00', NULL),
(7, 'Kontakty', 'http://corporate.local/contacts', '0', '2020-03-02 19:32:19', NULL),
(8, 'Link', 'link.ru', '0', '2020-04-15 11:08:24', '2020-04-15 11:08:24');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_02_174346_create_articles_table', 1),
(5, '2020_03_02_175538_create_portfolios_table', 1),
(6, '2020_03_02_175859_create_filters_table', 1),
(7, '2020_03_02_180512_create_comments_tabel', 1),
(8, '2020_03_02_181335_create_sliders_table', 1),
(9, '2020_03_02_181626_create_menus_table', 1),
(10, '2020_03_02_184912_create_categories_table', 1),
(11, '2020_03_02_185400_change_articles_table', 1),
(12, '2020_03_02_190027_change_comments_tabel', 1),
(13, '2020_03_02_191902_change_portfolios_table', 2),
(14, '2020_03_02_194112_chnage_articles_table', 3),
(15, '2020_03_02_195450_change_articles_table2', 4),
(16, '2020_03_02_202004_change_portfiolios_table', 5),
(17, '2020_03_22_125051_change_articles_table_3', 6),
(18, '2020_03_22_182054_change_portfolios_table2', 7),
(19, '2020_03_26_190300_change_users_table', 8),
(28, '2020_03_30_180615_create_roles_table', 9),
(29, '2020_03_30_180733_create_permissions_table', 9),
(30, '2020_03_30_180857_create_permission_role_table', 9),
(31, '2020_03_30_181004_create_role_user_table', 9),
(32, '2020_03_30_181631_cange_role_user_table', 9),
(33, '2020_03_30_182120_change_permission_r_ole_table', 9),
(34, '2020_04_07_113706_change_articles_table4', 10),
(35, '2020_04_09_105841_change_users_table-2', 10),
(36, '2020_04_09_112033_change_comments_table2', 11),
(37, '2020_04_09_112356_change_comments_table3', 12),
(38, '2020_04_15_160718_change_menus_table2', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'VIEW_ADMIN', NULL, NULL),
(3, 'VIEW_ARTICLES', NULL, NULL),
(4, 'ADD_ARTICLES', NULL, NULL),
(5, 'UPDATE_ARTICLES', NULL, NULL),
(6, 'VIEW_USERS', NULL, NULL),
(7, 'EDIT_USERS', NULL, NULL),
(8, 'DELETE_ARTICLES', NULL, NULL),
(9, 'VIEW_ROLES', NULL, NULL),
(10, 'EDIT_ROLES', NULL, NULL),
(11, 'VIEW_ADMIN_MENU', NULL, NULL),
(12, 'EDIT_MENUS', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `permission_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`id`, `created_at`, `updated_at`, `role_id`, `permission_id`) VALUES
(11, NULL, NULL, 1, 4),
(12, NULL, NULL, 1, 8),
(13, NULL, NULL, 1, 10),
(14, NULL, NULL, 1, 7),
(15, NULL, NULL, 1, 5),
(16, NULL, NULL, 1, 2),
(17, NULL, NULL, 1, 3),
(18, NULL, NULL, 1, 9),
(19, NULL, NULL, 1, 6),
(20, NULL, NULL, 2, 5),
(21, NULL, NULL, 3, 2),
(22, NULL, NULL, 2, 2),
(23, NULL, NULL, 2, 3),
(24, NULL, NULL, 1, 11),
(25, NULL, NULL, 1, 12),
(26, NULL, NULL, 2, 6),
(27, NULL, NULL, 2, 4),
(28, NULL, NULL, 3, 3),
(29, NULL, NULL, 3, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `portfolios`
--

CREATE TABLE `portfolios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `filter_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `portfolios`
--

INSERT INTO `portfolios` (`id`, `title`, `text`, `customer`, `alias`, `created_at`, `updated_at`, `filter_alias`, `image`, `keywords`, `meta_desc`) VALUES
(1, 'Steep This!', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n				                                <p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum,<strong> sem lacus ultrice</strong>s est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.</p>\r\n				                                <p>Sed <a href=\"http://yourinspirationweb.com/demo/sheeva/work/xmas-icons/#\">porttitor eros </a>ut purus elementum a consectetur purus vulputate</p>', 'Steep This!', 'project1', '2020-03-01 06:44:14', NULL, 'brand-identity', '{\"mini\":\"0081-175x175.jpg\",\"max\":\"0027.jpg\",\"first\":\"0081-385x192.jpg\",\"path\":\"0027.jpg\"}\r\n\r\n', 'Steep This!', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>'),
(2, 'Guanacos', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n				                                <p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum,<strong> sem lacus ultrice</strong>s est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.</p>\r\n				                                <p>Sed <a href=\"http://yourinspirationweb.com/demo/sheeva/work/xmas-icons/#\">porttitor eros </a>ut purus elementum a consectetur purus vulputate</p>', 'Guanacos', 'prohect2', '2020-03-01 19:41:00', NULL, 'web-design', '{\"mini\":\"0071-175x175.jpg\",\"max\":\"0071-770x368.jpg\",\"first\":\"0071-320x154.jpg\",\"path\":\"0071.jpg\"}\r\n', 'Guanacos', 'Guanacos'),
(3, 'Nili Studios', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n				                                <p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum,<strong> sem lacus ultrice</strong>s est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.</p>\r\n				                                <p>Sed <a href=\"http://yourinspirationweb.com/demo/sheeva/work/xmas-icons/#\">porttitor eros </a>ut purus elementum a consectetur purus vulputate</p>', 'Nili Yavin', 'project-3', '2020-03-03 07:23:46', NULL, 'web-development', '{\"mini\":\"009-175x175.jpg\",\"max\":\"009-770x368.jpg\",\"first\":\"009-320x154.jpg\",\"path\":\"009.jpg\"}\r\n', 'Nili Studios', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>'),
(4, 'Love', '<p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n<p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum, sem lacus ultrices est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.\r\n\r\nSed porttitor eros ut purus elementum a consectetur purus vulputate</p>', 'Customer-4', 'project4', '2020-03-01 19:00:00', NULL, 'web-design', '{\"mini\":\"0027-175x175.jpg\",\"max\":\"0013.jpg\",\"first\":\"0013-320x154.jpg\",\"path\":\"0013.jpg\"}\r\n', 'Love', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>'),
(5, 'Kineda', '<p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n<p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum, sem lacus ultrices est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.\r\n\r\nSed porttitor eros ut purus elementum a consectetur purus vulputate</p>', 'customer-5', 'project5', '2020-03-02 19:32:19', NULL, 'brand-identity', '{\"mini\":\"0071-175x175.jpg\",\"max\":\"0072.jpg\",\"first\":\"0072-320x154.jpg\",\"path\":\"0072.jpg\"}\r\n', 'Kineda', ''),
(6, 'Octopus', '<p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n<p>\r\nDonec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum, sem lacus ultrices est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.\r\n</p>\r\n', 'customer-6', 'project6', '2020-03-09 19:00:00', NULL, 'web-development', '{\"mini\":\"0043-175x175.jpg\",\"max\":\"0052-770x368.jpg\",\"first\":\"0052-320x154.jpg\",\"path\":\"0052.jpg\"}\r\n\r\n', '', ''),
(7, 'Miller Bob', '<p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n<p>\r\nDonec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum, sem lacus ultrices est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.\r\n</p>\r\n', 'customer-7', 'projetc-7', '2020-03-03 01:45:18', NULL, 'web-design', '{\"mini\":\"0043-175x175.jpg\",\"max\":\"0043-770x368.jpg\",\"first\":\"0043-320x154.jpg\",\"path\":\"0043.jpg\"}\r\n', '', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>'),
(8, 'Nili Studios', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n				                                <p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum,<strong> sem lacus ultrice</strong>s est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.</p>\r\n				                                <p>Sed <a href=\"http://yourinspirationweb.com/demo/sheeva/work/xmas-icons/#\">porttitor eros </a>ut purus elementum a consectetur purus vulputate</p>', 'customer-8', 'project-8', '2020-03-01 19:00:00', NULL, 'web-design', '{\"mini\":\"0043-175x175.jpg\",\"max\":\"0071-770x368.jpg\",\"first\":\"0071-320x154.jpg\",\"path\":\"0071.jpg\"}\r\n', 'Nili Studios', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>'),
(9, 'Guanacos', ' <p>Nullam volutpat, mauris scelerisque iaculis semper, justo odio rutrum urna, at cursus urna nisl et ipsum. Donec dapibus lacus nec sapien faucibus eget suscipit lorem mattis.</p>\r\n				                                <p>Donec non mauris ac nulla consectetur pretium sit amet rhoncus neque. Maecenas aliquet, diam sed rhoncus vestibulum,<strong> sem lacus ultrice</strong>s est, eu hendrerit tortor nulla in dui. Suspendisse enim purus, euismod interdum viverra eget, ultricies eu est. Maecenas dignissim mauris id est semper suscipit. Suspendisse venenatis vestibulum quam, quis porttitor arcu vestibulum et.</p>\r\n				                                <p>Sed <a href=\"http://yourinspirationweb.com/demo/sheeva/work/xmas-icons/#\">porttitor eros </a>ut purus elementum a consectetur purus vulputate</p>', 'customer-9', 'project9', '2020-03-01 01:45:18', NULL, 'web-development', '{\"mini\":\"0043-175x175.jpg\",\"max\":\"00311.jpg\",\"first\":\"00311-320x154.jpg\",\"path\":\"00311.jpg\"}\r\n', 'Guanacos', '');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL),
(2, 'Moderator', NULL, NULL),
(3, 'Guest', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `role_user`
--

INSERT INTO `role_user` (`id`, `created_at`, `updated_at`, `user_id`, `role_id`) VALUES
(1, NULL, NULL, 2, 1),
(2, NULL, NULL, 4, 2),
(3, NULL, NULL, 3, 3),
(16, NULL, NULL, 10, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, ' <h2 style=\"color:#fff\">CORPORATE, MULTIPURPOSE.. <br /><span>PINK RIO</span></h2>', '<p>Nam id quam a odio euismod pellentesque. Etiam congue rutrum risus non vestibulum. Quisque a diam at ligula blandit consequat. Mauris ac mi velit, a tempor neque</p>', 'xx.jpg', '2020-03-03 07:23:46', NULL),
(2, '<h2 style=\"color:#fff\">PINKRIO. <span>STRONG AND POWERFUL.</span></h2>', '<p>Nam id quam a odio euismod pellentesque. Etiam congue rutrum risus non vestibulum. Quisque a diam at ligula blandit consequat. Mauris ac mi velit, a tempor neque</p>', '00314.jpg', '2020-03-03 01:45:18', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `login`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Guvanch', 'guvanch', 'guvanchhojamov@gmail.com', NULL, '$2y$10$yraULiEnng68A1GqqKgroOFAJTPrXDdbizmlHoe8mZ3YpRM9TM2Jm', NULL, '2020-03-04 07:50:39', '2020-03-04 07:50:39'),
(3, 'User', 'user', 'user@mail.ru', NULL, '$2y$10$4d4BpA9bWWsIGpDm31kdquQwMe8PTFINP0zvXyZw/r3W2WqXrEGB6', NULL, '2020-03-26 11:03:25', '2020-03-26 11:03:25'),
(4, 'Moderator', 'moderator', 'moderator@mail.ru', NULL, '$2y$10$YFFw12o2fhlkA2EoqVkGzuSxVnErnoDiO4x00RWwTzxHniorPEG3a', NULL, '2020-03-26 11:07:35', '2020-03-26 11:07:35'),
(10, 'Testdsdasdadsadadadada', 'test', 'test@mail.ru', NULL, '$2y$10$FvUXgRYX0dLkjmTl6QbrB.1B5NZJM/gGm2MDUpnONmd/0KswSHNaq', NULL, '2020-04-24 07:06:53', '2020-04-24 07:22:07');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_alias_unique` (`alias`),
  ADD KEY `articles_user_id_foreign` (`user_id`),
  ADD KEY `articles_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_alias_unique` (`alias`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_article_id_foreign` (`article_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filters`
--
ALTER TABLE `filters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `filters_alias_unique` (`alias`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`);

--
-- Индексы таблицы `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `portfolios_alias_unique` (`alias`),
  ADD KEY `portfolios_filter_alias_foreign` (`filter_alias`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `filters`
--
ALTER TABLE `filters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `portfolios`
--
ALTER TABLE `portfolios`
  ADD CONSTRAINT `portfolios_filter_alias_foreign` FOREIGN KEY (`filter_alias`) REFERENCES `filters` (`alias`);

--
-- Ограничения внешнего ключа таблицы `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

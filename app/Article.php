<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable=['id','title','full_text','alias','image','created_at',
                         'updated_at','user_id','category_id','description','keywords','meta_desc'];
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function comment(){
        return $this->hasMany('App\Comment');
    }
    public function comments(){
        return $this->hasMany('App\Comment');
    }
}

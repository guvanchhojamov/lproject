<?php

namespace App\Http\Controllers;

use Lavary\Menu;
use App\Repositoreis\MenusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class SiteController extends Controller
{
    //
    protected $p_rep; //portfolio_repository
    protected $s_rep; //slides_repository
    protected $a_rep; //articles_repository
    protected $m_rep; //menu_repository
    protected $c_rep; //comments repository

    protected $template;
    protected $vars;

    protected $contentRightBar = FALSE;
    protected $contentLeftBar = FALSE;
    protected $keywords;
    protected $meta_desc;
    protected $title;

    protected $bar='no';

    public function __construct(MenusRepository $m_rep){
        $this->m_rep=$m_rep;
    }
    protected function renderOutPut(){
        $menu = $this->getMenu();
        //dd($menu);
        $header = view(env('THEME','pink').'.partials.header')->with('menu',$menu)->render();
        $this->vars = Arr::add($this->vars,'header',$header);

        if ($this->contentRightBar){
            $rightBar = view(env('THEME','pink').'.rightBar')->with('content_rightBar',$this->contentRightBar)->render();
            $this->vars =Arr::add($this->vars,'rightBar',$rightBar);
        }
        $this->vars=Arr::add($this->vars,'bar',$this->bar);

        $footer=view(env('THEME','pink').'.footer')->render();
        $this->vars=Arr::add($this->vars,'footer',$footer);

        $this->vars=Arr::add($this->vars,'keywords',$this->keywords);
        $this->vars=Arr::add($this->vars,'meta_desc',$this->meta_desc);
        $this->vars=Arr::add($this->vars,'title',$this->title);

        return view($this->template)->with($this->vars);
    }
    protected function getMenu(){
        $menu = $this->m_rep->get();
          //dd($menu);
        $mBuilder = Menu\Facade::make('MyNav',function ($m) use ($menu){
          foreach ($menu as $item){
              if ($item->parent == 0){
                  $m->add($item->title,$item->url_link)->id($item->id);
              }else{
                  if ($m->find($item->parent)){
                      $m->find($item->parent)->add($item->title,$item->url_link)->id($item->id);
                  }
              }
          }
        });
        //dd($mBuilder);
        return $mBuilder;
    }
}

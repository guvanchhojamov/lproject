<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Repositoreis\MenusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactsController extends SiteController
{
    //
    public function __construct()
    {
        parent::__construct(new MenusRepository(new Menu()));

        $this->bar = config('SettingsCustom.contacts_bar_side');
        $this->template=env('THEME','pink').'.contacts';


    }

    public function index(Request $request){
        if ($request->isMethod('post')){
            $data=$request->except(['_token']);

            $messages = [
                'required'=>trans('ru.err_required'),
                'email'=>trans('ru.err_email'),
                'string'=>trans('ru.err_string'),
            ];

            $validator = Validator::make($data,[
                'name'=> 'required|max:255',
                'email'=>'required|email|max:255',
                'message'=>'required',
            ],$messages);

            if ($validator->fails()){
                return redirect()->route('contacts')->withErrors($validator)->withInput();
            }
            $result = Mail::send(env('THEME','pink').'.email',['data'=>$data], function ($m) use ($data){
                $mail_admin = env('MAIL_ADMIN',config('SettingsCustom.mail_admin'));
                $m->from($data['email'],$data['name']);
                $m->to($mail_admin,'Mr.Admin')->subject('Taze gelen habar!');
            });

                return redirect()->route('contacts')->with('status',trans('ru.email_send_success'));

        }

        $this->title='Kontaktlar sahypasy Pink Rio';
        $this->keywords='Kontaktlar sahypasy Pink Rio';
        $this->meta_desc='Pink rio kontaktlar meta desc parametri';

        $content=view(env('THEME','pink').'.contacts_content')->render();

        $this->contentRightBar = view(env('THEME','pink').'.contactsBar')->render();
        $this->vars=Arr::add($this->vars,'content',$content);

        return $this->renderOutPut();
    }


}

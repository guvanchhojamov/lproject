<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositoreis\ArticlesRepository;
use App\Repositoreis\MenusRepository;
use App\Repositoreis\PortfoliosRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Lavary\Menu;

class MenusController extends AdminController
{
    protected $m_rep;
    public function __construct(MenusRepository $m_rep, ArticlesRepository $a_rep, PortfoliosRepository $p_rep)
    {
        parent::__construct();

        $this->a_rep=$a_rep;
        $this->p_rep=$p_rep;
        $this->m_rep=$m_rep;
        $this->template=env('THEME','pink').'.admin.menus';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Gate::denies('VIEW_ADMIN_MENU')){
            abort(403);
        }
        $menu=$this->getMenus();
        $this->content=view(env('THEME','pink').'.admin.menus_content')->with('menus',$menu)->render();

        return $this->renderOutput();
    }

    public function getMenus(){
        $menu=$this->m_rep->get();
        if ($menu->isEmpty()){
            return FALSE;
        }

       $adminMenu= Menu\Facade::make('forMenuPart', function($m) use ($menu){
            foreach($menu as $item){
                if($item->parent==0){
                    $m->add($item->title,$item->url_link)->id($item->id);
                }else{
                    if ($m->find($item->parent)){
                        $m->find($item->parent)->add($item->title,$item->url_link)->id($item->id);
                    }
                }
            }
        });
        //dd($adminMenu);
        return $adminMenu;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->title='Taze menu doretmek';
        $this->content=view(env('THEME','pink').'.admin.menu_create_content')->render();

       return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (Gate::denies('EDIT_MENUS')){
            abort(403);
        }
        $result = $this->m_rep->addMenu($request);
        if (is_array($result) && !empty($result['errors'])){
            return redirect()->back()->with($result);
        }
        return redirect('/admin/menus')->with($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = \App\Menu::where('id',$id);
        $menu = $data->get();
        $menu = $menu[0];
        //dd($menu);
        $this->title='Menu uytgetmek - '.$menu['title'];
        $this->content=view(env('THEME','pink').'.admin.update_menu_content')
                        ->with('menu',$menu)->render();
        return $this->renderOutput();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (Gate::denies('EDIT_MENUS')){
            abort(403);
        }
        $menu=\App\Menu::where('id',$id)->first();
        $result=$this->m_rep->updateMenu($request,$menu);
        if (is_array($result) && !empty($result['errors'])){
           return redirect()->back()->with($result);
        }
        return redirect('/admin/menus')->with($result);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $menu=\App\Menu::where('id',$id)->first();
        $result =$this->m_rep->deleteMenu($menu);

        if (is_array($result) && !empty($result['errors'])){
            return redirect()->back()->with($result);
        }
        return redirect('/admin/menus')->with($result);

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Repositoreis\ArticlesRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;

class ArticlesController extends AdminController
{
    public function   __construct(ArticlesRepository $a_rep)
    {
       $this->a_rep=$a_rep;
        parent::__construct();

        $this->template = env('THEME','pink').'.admin.articles';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->title = 'Artikler sahypasy';

        if (Gate::denies('VIEW_ARTICLES')){
            abort(403);
        }
        $articles=$this->getArticles();
        $this->content=\view(env('THEME','pink').'.admin.articles_content')->with('articles',$articles)->render();
        return $this->renderOutput();
    }

    public function getArticles(){
        return $this->a_rep->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Gate::denies('create',new Article())){
            abort(403);
        }

        $this->title='Taze statya goshmak';

        $categories=Category::select(['id','title','parent_id','alias']);
        $categories=$categories->get();

        $lists = [];

        foreach ($categories as $category){
            if ($category->parent_id == 0){
                $lists[$category->title]=[];
            }else{
                $lists[$categories->where('id',$category->parent_id)->first()->title][$category->id]=$category->title;
            }
        }

        $this->content=\view(env('THEME','pink').'.admin.article_create_content')->with('categories',$lists)->render();
        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        //
        $result = $this->a_rep->addArticle($request);
        if (is_array($result) && !empty($result['errors'])){
            return redirect()->back()->with($result);
        }
        return redirect('/admin/aarticles')->with($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($alias)
    {
        //
        $article=Article::where('alias',$alias)->first();
        if (Gate::denies('update', new Article())){
            abort(403);
        }

        $this->title=$article->title.'-materiyaly uytgetmek!';

        $this->title='Taze statya goshmak';

        $categories=Category::select(['id','title','parent_id','alias']);
        $categories=$categories->get();

        $lists = [];

        foreach ($categories as $category){
            if ($category->parent_id == 0){
                $lists[$category->title]=[];
            }else{
                $lists[$categories->where('id',$category->parent_id)->first()->title][$category->id]=$category->title;
            }
        }
        $article->image=json_decode($article->image);

        $this->content=\view(env('THEME','pink').'.admin.update_article_content')->with(['article'=>$article,'categories'=>$categories])->render();

        return $this->renderOutput();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $alias)
    {
        //
        $article = Article::where('alias',$alias)->first();
        $result = $this->a_rep->updateArticle($request,$article);
        if (is_array($result) && !empty($result['errors'])){
            return redirect()->back()->with($result);
        }
        return redirect('/admin/aarticles')->with($result);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($alias)
    {
        //
        $article = Article::where('alias',$alias)->first();

        $result = $this->a_rep->deleteArticle($article);
        if (is_array($result) && !empty($result['errors'])){
            return redirect()->back()->with($result);
        }
        return redirect('/admin/aarticles')->with($result);
    }
}

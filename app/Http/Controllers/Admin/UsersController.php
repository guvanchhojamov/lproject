<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Repositoreis\RolesRepository;
use App\User;
use Illuminate\Http\Request;
use App\Repositoreis\UsersRepository;
use Illuminate\Support\Facades\Gate;

class UsersController extends AdminController
{
      protected $rol_rep;
      protected $us_rep;

      public function __construct( RolesRepository $rol_rep, UsersRepository $us_rep){

        parent::__construct();
        $this->rol_rep=$rol_rep;
        $this->us_rep=$us_rep;
        $this->template=env('THEME','pink').'.admin.users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if ( Gate::denies('VIEW_USERS')){
            abort(403);
        }

        $this->title='Adminstratorlar Sahypasy';

        $users=$this->getUsers();

        $this->content=view(env('THEME','pink').'.admin.users_content')->with('users',$users)->render();
        return $this->renderOutput();
    }

    public function getUsers(){
        return $this->us_rep->get();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if ( Gate::denies('EDIT_USERS')){
            abort(403);
        }
        $this->title='Taze User goshmak';
        $roles=$this->getRoles();
        $this->content=view(env('THEME','pink').'.admin.user_create')->with('roles',$roles)->render();
        return $this->renderOutput();
    }

    public function getRoles(){
        return $this->rol_rep->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        //
        $result=$this->us_rep->addUser($request);
        if (is_array($result) && !empty($result['error'])){
            return back()->with($result);
        }
        return redirect('admin/users')->with($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user )
    {
        //
        $this->title=$user->name.'- Useri uytgetmek!';

        $roles=$this->getRoles();
        $this->content=view(env('THEME','pink').'.admin.update_user_content')->with(['user'=>$user,'roles'=>$roles])->render();

        return $this->renderOutput();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        //
        $result=$this->us_rep->updateUser($request, $user);
        if (is_array($result) && !empty($result['error'])){
            return back()->with($result);
        }
        return redirect('admin/users')->with($result);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $result=$this->us_rep->deleteUser($user);
        if (is_array($result) && !empty($result['error'])){
            return back()->with($result);
        }
        return redirect('admin/users')->with($result);

    }
}

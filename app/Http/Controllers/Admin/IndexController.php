<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Menu;
use App\Repositoreis\MenusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class IndexController extends AdminController
{
    //
public function __construct()
{
     $this->user=Auth::user();
     parent::__construct();
     $this->template=env('THEME','pink').'.admin.index';
}
public function index(){
    $this->user=Auth::user();
    if (Gate::denies('VIEW_ADMIN')){
        abort(403);
    }


    $this->title='Dolandyryjy paneli';
    return $this->renderOutput();
}

}

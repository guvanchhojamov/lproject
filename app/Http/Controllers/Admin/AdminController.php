<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositoreis\MenusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Lavary\Menu\Menu;

class AdminController extends Controller
{
    //
    protected $p_rep;
    protected $a_rep;
    protected $m_rep;
    protected $user;
    protected $template;
    protected $content = FALSE;
    protected $title;
    protected $vars;
    protected $admin_name;
    protected $admin_role;

public function __construct()
{
    //$this->user=Auth::user();
}
public function renderOutput(){
    $this->vars=Arr::add($this->vars,'title',$this->title);

    $this->user=Auth::user();

   if ($this->user){
    $menu = $this->getMenu();
   }else{
       $menu = '';
   }
    $header = view(env('THEME','pink').'.admin.partials.header')->with(['menu'=>$menu,'user'=>$this->user])->render();
    $this->vars=Arr::add($this->vars,'header',$header);

    if ($this->content){
        $this->vars=Arr::add($this->vars,'content',$this->content);
    }
    $footer = view(env('THEME','pink').'.admin.partials.footer')->render();
    $this->vars=Arr::add($this->vars,'footer',$footer);

    return view($this->template)->with($this->vars);
}

   public function getMenu(){
    return \Lavary\Menu\Facade::make('adminMenu',function($menu){
        $menu->add('Statyalar',array('route'=>'aarticles.index'));
        $menu->add('Portoliolar',array('route'=>'aarticles.index'));
        $menu->add('Menu',array('route'=>'menus.index'));
        $menu->add('Userler',array('route'=>'users.index'));
        $menu->add('Priweligiyalar',array('route'=>'permissions.index'));
        $menu->add('Kommentariyalar',array('route'=>'aarticles.index'));
            });

   }

}

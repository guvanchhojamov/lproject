<?php

namespace App\Http\Controllers;

use App\Category;
use App\Menu;
use App\Repositoreis\ArticlesRepository;
use App\Repositoreis\CommentsRepository;
use App\Repositoreis\MenusRepository;
use App\Repositoreis\PortfoliosRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class ArticlesController extends SiteController
{
    public function __construct(PortfoliosRepository $p_rep, ArticlesRepository $a_rep, CommentsRepository $c_rep)
    {
        $this->p_rep=$p_rep;
        $this->a_rep=$a_rep;
        $this->c_rep=$c_rep;
        parent::__construct(new MenusRepository(new Menu()));
        $this->bar=Config::get('SettingsCustom.bar_side_on_articles');
        $this->template=env('THEME','pink').'.articles';

//        $this->keywords='Articles page,Pink Rio template';
//        $this->meta_desc='Bu sahypa Pink rio templatenin Blog, Articles sahypasy';
//        $this->title='Articles Pink Rio';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cat_alias=FALSE)
    {
        //
        $articles = $this->getArticles($cat_alias,Config::get('SettingsCustom.articles_per_page'));

        $this->title = 'Pink rio | Blog';
        $this->keywords = 'Blog keywords';
        $this->meta_desc = 'Blog meta desc';

        $content=view(env('THEME','pink').'.articles_content')->with('articles',$articles)->render();
        $this->vars=Arr::add($this->vars,'content',$content);

        $comments=$this->getComments(\config('SettingsCustom.count_comment_right_bar'));
        $portfolios=$this->getPortfolios(\config('SettingsCustom.count_portfolio_right_bar'));

        $this->contentRightBar=view(env('THEME','pink').'.articlesBar')->with(
                                     ['comments'=>$comments, 'portfolios'=>$portfolios])->render();
        return $this->renderOutPut();
    }

    protected function getComments($take){
        $comments=$this->c_rep->get(['id','name','email','site','text','created_at','user_id','article_id'],$take);
        if ($comments){
            $comments->load('user','article');
        }
        return $comments;

    }
    protected function getPortfolios($take){
        $portfolios=$this->p_rep->get(['id','title','text','alias','image','filter_alias'],$take);
        return $portfolios;
    }

    protected function getArticles($alias=FALSE,$paginate){
        $where=FALSE;
        if ($alias){
         $id = Category::select('id')->where('alias',$alias)->first()->id;
            $where = ['category_id',$id];
        }
        $articles = $this->a_rep->get(['id','title','description','alias','image','created_at','category_id','user_id','full_text','keywords','meta_desc'],FALSE,$paginate,$where);
        if ($articles){
            $articles->load('user','category','comment');
        }
        return $articles;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($alias=FALSE)
    {
        //
        $article = $this->a_rep->one($alias,['comments'=>TRUE]);
        $this->title = $article->title;
        $this->keywords = strip_tags($article->keywosrds);
        $this->meta_desc = strip_tags($article->meta_desc);

        if ($article){
            $article->image=json_decode($article->image);
        }
       // dd($article->comments->groupBy('parent_id'));

        $content = view(env('THEME','pink').'.article_on_details')->with('article',$article)->render();
        $this->vars=Arr::add($this->vars,'content',$content);

        $comments=$this->getComments(\config('SettingsCustom.count_comment_right_bar'));
        $portfolios=$this->getPortfolios(\config('SettingsCustom.count_portfolio_right_bar'));

        $this->contentRightBar=view(env('THEME','pink').'.articlesBar')->with(
            ['comments'=>$comments, 'portfolios'=>$portfolios])->render();

        return $this->renderOutPut();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

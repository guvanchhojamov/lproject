<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Portfolio;
use App\Repositoreis\ArticlesRepository;
use App\Repositoreis\MenusRepository;

use App\Repositoreis\PortfoliosRepository;
use App\Repositoreis\SlidersRepository;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;



class IndexController extends SiteController
{
     public function __construct(SlidersRepository $s_rep, PortfoliosRepository $p_rep, ArticlesRepository $a_rep)
     {
         $this->p_rep=$p_rep;
         $this->s_rep=$s_rep;
         $this->a_rep=$a_rep;
         parent::__construct(new MenusRepository(new Menu()));
         $this->bar=Config::get('SettingsCustom.bar_side_on_index');
         $this->template=env('THEME','pink').'.index';

     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //index portfolios
        $portfolios = $this->getPortfolio();
        $content = view(env('THEME','pink').'.content')->with('portfolios',$portfolios)->render();
        $this->vars = Arr::add($this->vars,'content',$content);
        //index sliders
        $sliderItems = $this->getSlider();
        $sliders = view(env('THEME','pink').'.sliders')->with('slider',$sliderItems)->render();
        $this->vars = Arr::add($this->vars,'sliders',$sliders);

        $articles=$this->getArticles();
        $this->contentRightBar =view(env('THEME','pink').'.indexBar')->with('articles',$articles)->render();

        $this->keywords='Home page,Pink Rio template';
        $this->meta_desc='Bu sahypa Pink rio templatenin esasy sahypasy';
        $this->title='Homepage Pink Rio';

        return $this->renderOutPut();
    }
    protected function getArticles(){
       $articles=$this->a_rep->get(['title','alias','image','created_at'],Config::get('SettingsCustom.count_article_items'));
       return $articles;
    }

    protected function getPortfolio(){
        $portfolio = $this->p_rep->get('*',Config::get('SettingsCustom.count_portfolio_items'));
        return $portfolio;
    }

    public function getSlider(){
       $sliders = $this->s_rep->get();
       //Dlya togo chtoby dobavit directoriyu(papku) kartinki
       if ($sliders->isEmpty()){
           return FALSE;
       }
       $sliders->transform(function($item,$key){
        $item->image = Config::get('SettingsCustom.slider_path').'/'.$item->image;
         return $item;
       });
       //end
       return $sliders;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

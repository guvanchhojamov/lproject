<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Repositoreis\MenusRepository;
use App\Repositoreis\PortfoliosRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class PortfolioController extends SiteController
{
    public function __construct(PortfoliosRepository $p_rep)
    {
        $this->p_rep = $p_rep;
        parent::__construct(new MenusRepository(new Menu()));
        $this->bar = 'no';
        $this->template=env('THEME','pink').'.portfolios';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $portfolios = $this->getPortfolios(FALSE,Config::get('SettingsCustom.portfolios_per_page'));

        $content = view(env('THEME','pink').'.portfolios_content')->with('portfolios',$portfolios)
            ->render();
        $this->vars =Arr::add($this->vars,'content',$content);
        return $this->renderOutPut();

    }

    protected function getPortfolios($take=FALSE,$paginate=FALSE){
        $portfolios = $this->p_rep->get('*',$take,$paginate);
        if ($portfolios){
            $portfolios->load('filter');
        }
        return $portfolios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($alias)
    {
        //

        $portfolio=$this->p_rep->one($alias);
        $portfolio->image = json_decode($portfolio->image);
        $this->keywords =$portfolio->keywords;
        $this->meta_desc=$portfolio->meta_desc;
        $this->title=$portfolio->title;

        $portfolios = $this->getPortfolios(\config('SettingsCustom.portfolios_count_on_detail_page'),FALSE);
        //dd($portfolios);
        $content = view(env('THEME','pink').'.portfolio_on_details')->with(['portfolios'=>$portfolios,'portfolio'=>$portfolio])->render();
        $this->vars = Arr::add($this->vars,'content',$content);
        return $this->renderOutPut();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

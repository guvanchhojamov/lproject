<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo ='/admin';

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/'.config('SettingsCustom.auth_redirect_url');
    }
    public function username()
    {
//        dd(config('SettingsCustom.auth_field_for_login'));
        return config('SettingsCustom.auth_field_for_login');
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    public function showLoginForm()
    {
       $header = view(env('THEME','pink').'.partials.header')->with('menu','');
       $footer = view(env('THEME','pink').'.footer');
        return view(env('THEME','pink').'.login')->with('title',trans('ru.login_title'))
             ->with('keywords',null)->with('meta_desc',null)->with('bar',null)->with('header',$header)->with('footer',$footer);
    }

}

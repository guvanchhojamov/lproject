<?php

namespace App\Http\Requests;

use App\Article;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use function PHPSTORM_META\type;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->canDo('ADD_ARTICLES');
    }

    protected function getValidatorInstance()
    {
        $validator=parent::getValidatorInstance(); // TODO: Change the autogenerated stub

        $validator->sometimes('alias','unique:articles|max:255',function ($input){
            $article = Article::where('alias',$input->alias)->first();
            if ( isset($article->id) && ($article->id == $input->update_id) && !empty($input->alias) ){    //Input->update_id integere owurmeli
                 return FALSE;
             }
            return !empty($input->alias); // yesli ne pustoe znachenie alias.Provoditsya proverka.

        });
        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'=>'required|max:255',
            'description'=>'required',
            'full_text'=>'required',
            'category_id'=>'required|integer',
        ];
    }
}

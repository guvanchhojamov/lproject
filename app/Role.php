<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    public function users(){
       return $this->belongsToMany('App\User','role_user');
    }

    public function permissions(){
        return $this->belongsToMany('App\Permission','permission_role');
    }


    public  function hasPermission($name,$require=FALSE){
        if (is_array($name)){
            foreach ($name as $nam){
                $nam = $this->hasPermission($nam);
                if ($nam && !$require){
                    return TRUE;
                }elseif (!$nam && $require){
                    return FALSE;
                }
            }
            return $require;
        }else{
            foreach ($this->permissions as $permission){
            if ($permission->name === $name){
                return true;
                }
            }
        }
        return FALSE;

    }

    public function savePermissions($inputPermissions){
         if (!empty($inputPermissions)){
             $this->permissions()->sync($inputPermissions);
         }else{
             $this->permissions()->detach();
         }
         return TRUE;
    }

}

<?php
namespace  App\Repositoreis;
use App\Permission;
use Illuminate\Support\Facades\Gate;
use App\Repositoreis\RolesRepository;
class PermissionsRepository extends Repository {

    public function __construct(Permission $permission, RolesRepository $rol_rep){
        $this->model = $permission;
        $this->rol_rep=$rol_rep;
    }

    public function changePermissions($request){

        if (Gate::denies('change', $this->model)){
            abort(403);
        }

        $data=$request->except('_token','re_priv_button');
        $roles=$this->rol_rep->get();

        foreach ($roles as $role) {
          if (isset($data[$role->id])){
             $role->savePermissions($data[$role->id]);
          }else{
              $role->savePermissions([]);
          }
        }
        return ['status'=>'Prawalar uytgedildi!'];
    }
}

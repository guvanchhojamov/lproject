<?php
   namespace  App\Repositoreis;
   use App\Menu;
   use Illuminate\Support\Facades\Validator;

   class MenusRepository extends Repository {

        public function __construct(Menu $menu){
          $this->model = $menu;
        }

      public function addMenu($request){
            $data=$request->except('_token','create_button');

           $message=[
               'required'=>'Ahli oyjukleri dolduryn!',
               'max:100'=>'Girizen maglumatynyz oran uzyn!'
           ];
           $validator = Validator::make($data,[
                'title'=>'required|max:100',
                'url_link'=>'required|max:155',
            ],$message);

           if ($validator->fails()){
               return ['errors'=>$validator->errors()->all()];
           }

           $this->model->fill($data);
           if($this->model->save($data)){
            return session()->put('status','Menu goshuldy!');
           }
      }

      public function updateMenu($request, $menu){
           $data=$request->except('_token','_method','update_button');
           $message =[
               'required'=>'Oyjukler bosh bolmaly dal!'
           ];
           $validator=Validator::make($data,[
               'title'=>'required|max:100',
               'url_link'=>'required|max:150'
           ], $message);

           if($validator->fails()){
              return ['errors'=>$validator->errors()->all()];
           }

           $menu->fill($data);
           if ($menu->update()){
               return session()->put('status','Menu uytgelidi!');
           }

      }

     public function deleteMenu($menu){
        if ($menu){
            if ($menu->delete()){
                return session()->put('status','Menu udalit edildi');
            }
        }
     }

   }
?>
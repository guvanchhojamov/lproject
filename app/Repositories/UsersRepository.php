<?php
  namespace App\Repositoreis;

  use App\User;
  use Illuminate\Support\Facades\Gate;
  use Illuminate\Support\Facades\Validator;

  class UsersRepository extends Repository
  {

      public function __construct(User $user)
      {
          $this->model = $user;
      }

      public function addUser($request)
      {
          if (Gate::denies('create', $this->model)) {
              abort(403);
          }
          $data = $request->except('_token', 'create_button');

//   dd($data);
          $user = $this->model::create([
              'name' => $data['name'],
              'login' => $data['login'],
              'email' => $data['email'],
              'password' => bcrypt($data['password']),
          ]);

          if ($user){
              $user->roles()->attach($data['role_id']);
          }else{
              return ['error'=>'Yalnyshlyk yuze chykdy. Tazeden synyanshyn.'];
          }
          return ['status'=>'User goshuldy!'];
      }

      public function updateUser($request, $user){
          if (Gate::denies('update',$this->model)){
              abort(403);
          }
          $data=$request->except('_token','_method','update_button');
          if (!empty($data['password'])){
                  $data['password']=bcrypt($data['password']);
          }else{
              $data=$request->except('_token','_method','update_button','password');
          }

          $user->fill($data)->update();
          $user->roles()->sync([$data['role_id']]);

          return ['status'=>'User uytgedildi!'];
      }

      public function deleteUser($user){
         if (Gate::denies('update',$this->model)){
             abort(403);
         }
         $user->roles()->detach();
         if ($user->delete()){
             return ['status'=>'User yok edildi!'];
         }
      }
  }
  ?>
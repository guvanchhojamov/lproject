<?php
   namespace  App\Repositoreis;
   use App\Article;
   use Illuminate\Support\Facades\Gate;
   use Illuminate\Support\Str;
   use Intervention\Image\Facades\Image;

   Class ArticlesRepository extends Repository{
       public function __construct(Article $article){
       $this->model=$article;
       }
       public function one($alias,$attr=array()){
           $article=parent::one($alias,$attr); // TODO: Change the autogenerated stub

           if ($alias && !empty($attr)){
               $article->load('comments');
               $article->comments->load('user');
           }
           return $article;
       }

       public function addArticle($request){

           if (Gate::denies('create',$this->model)){
               abort(403);
           }
           $data = $request->except(['_token','create_button','image']);


           if (empty($data['alias'])){
               $data['alias']=$this->transliteration($data['title']);
           }elseif(!empty($data['alias'])){
               $data['alias']=$this->transliteration($data['alias']);
           }


           if ($this->one($data['alias'],FALSE)){
               $request->merge(['alias'=>$data['alias']]);
               $request->flash();
               return ['errors'=>['Bu psevdonim(alias) eyyam bar, bashga saylan!']];
           }

           if ($request->hasFile('image')){
               $image=$request->file('image');
               if ($image->isValid()){
                   $str=Str::random(8);
                   $obj=new \stdClass();

                   $obj->mini=$str.'_mini.jpg';
                   $obj->max=$str.'_max.jpg';
                   $obj->path=$str.'.jpg';

                   $img=Image::make($image);

                   $path=public_path().'/'.env('THEME','pink').'/assets/images/articles/';


                   $img->fit(config('SettingsCustom.image.width',1024),config('SettingsCustom.image.height',768))
                                ->save($path.$obj->path);
                   $img->fit(config('SettingsCustom.articles_img.max.width'),config('SettingsCustom.articles_img.max.height'))
                                ->save($path.$obj->max);
                   $img->fit(config('SettingsCustom.articles_img.mini.width'),config('SettingsCustom.articles_img.mini.height'))
                                ->save($path.$obj->mini);
               }

               $data['image']=json_encode($obj);
               $this->model->fill($data);
               if($request->user()->articles()->save($this->model)){
                   return session()->put('status','Taze materiyal goshuldy!');
               }

           }
       }

       public function updateArticle($request,$article){

           if (Gate::denies('update',$this->model)){
               abort(403);
           }
           $data = $request->except(['_token','create_button','image','_method']);

           if (empty($data['alias'])){
               $data['alias']=$this->transliteration($data['title']);
           }elseif(!empty($data['alias'])){
               $data['alias']=$this->transliteration($data['alias']);
           }

           $result =$this->one($data['alias'],FALSE);

           if ( isset($result->id) && ($result->id != $article->id)  ) {
               $request->merge(['alias'=>$data['alias']]);
               $request->flash();
               return ['errors'=>['Bu psevdonim(alias) eyyam bar, bashga saylan!']];
           }

           if ($request->hasFile('image')){
               $image=$request->file('image');
               if ($image->isValid()){
                   $str=Str::random(8);
                   $obj=new \stdClass();

                   $obj->mini=$str.'_mini.jpg';
                   $obj->max=$str.'_max.jpg';
                   $obj->path=$str.'.jpg';

                   $img=Image::make($image);

                   $path=public_path().'/'.env('THEME','pink').'/assets/images/articles/';


                   $img->fit(config('SettingsCustom.image.width',1024),config('SettingsCustom.image.height',768))
                       ->save($path.$obj->path);
                   $img->fit(config('SettingsCustom.articles_img.max.width'),config('SettingsCustom.articles_img.max.height'))
                       ->save($path.$obj->max);
                   $img->fit(config('SettingsCustom.articles_img.mini.width'),config('SettingsCustom.articles_img.mini.height'))
                       ->save($path.$obj->mini);
               }

               $data['image']=json_encode($obj);
           }
           $article->fill($data);
           if($article->update()){
               return session()->put('status','Materiyal uytgedildi!');
           }
       }

       public function deleteArticle($article){

           if ($article->image){
               $images = json_decode($article->image);
           }
           if ($images){
               foreach ($images as $img){
                   if (file_exists(public_path(env('THEME','pink').'/assets/images/articles/'.$img))){
                       unlink(public_path(env('THEME','pink').'/assets/images/articles/'.$img));
                   }
               }
           }
         if (Gate::denies('delete', $article)){
             abort(403);
         }
         $article->comments()->delete();

         if ($article->delete()){
             return ['status'=>'Materiyal yok edildi!'];
         }
       }


   }
?>
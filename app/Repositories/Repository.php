<?php
 namespace App\Repositoreis;

 use Config;
 use function GuzzleHttp\Psr7\str;

 //use Illuminate\Support\Facades;

 Abstract class Repository {
    protected $model;
    protected $per_page;

    public function  get($select='*',$take=FALSE, $paginate=FALSE ,$where = FALSE){
        $builder = $this->model->select($select);
        if ($take){
            $builder->take($take);
        }
        if (!empty($where)){
            $builder->where($where[0],$where[1]);
        }
        if ($paginate){
            //dd($paginate);
            $this->per_page = $paginate;
           return $this->check($builder->paginate($this->per_page));
        }
        return $this->check($builder->get());
    }

    protected function check($result){

        if ($result->isEmpty()){
            return FALSE;
        }
        $result->transform(function($item,$key){
            if (is_string($item->image) && is_object(json_decode($item->image)) && json_last_error() == JSON_ERROR_NONE){
                $item->image = json_decode($item->image);
            }
          return $item;
        });
        return $result;
    }

    public function one($alias,$attr = array()){
        $result = $this->model->where('alias',$alias)->first();
        return $result;
    }

    public function transliteration($string){
     $str=mb_strtolower($string,'utf-8');
     //A-Za-z0-9-
     $str = preg_replace('/(\s|[^A-Za-z0-9\-])+/','-',$str);

     $str=trim($str,'-');
     return $str;
    }
 }
?>
<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //@set($,10) example primer
        Blade::directive('set',function($exp){
         list($name,$val) = explode(',',$exp);
         return "<?php $name=$val ?>";
        });

//        DB::listen(function ($query){
//            echo "<p>".$query->sql."</p>";
//        });
    }


}

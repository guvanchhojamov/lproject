<?php

namespace App\Providers;

use App\Article;
use App\Permission;
use App\Policies\ArticlePolicy;
use App\Policies\PermissionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',

        //Goshulan Polices
        Article::class=>ArticlePolicy::class,
        Permission::class=>PermissionPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        Gate::define('VIEW_ADMIN', function ($user){
            return $user->canDo(['VIEW_ADMIN','VIEW_ARTICLES'], FALSE);
        });
        Gate::define('VIEW_ARTICLES', function ($user){
            return $user->canDo('VIEW_ARTICLES',FALSE);
        });
        Gate::define('VIEW_ROLES', function ($user){
            return $user->canDo('VIEW_ROLES',FALSE);
        });
        Gate::define('VIEW_ADMIN_MENU', function($user){
            return $user->canDo('VIEW_ADMIN_MENU',FALSE);
        });
        Gate::define('EDIT_MENUS', function ($user){
            return $user->canDo('EDIT_MENUS',FALSE);
        });
        Gate::define('VIEW_USERS', function ($user){
         return $user->canDo('VIEW_USERS',FALSE);
        });
        Gate::define('EDIT_USERS',function ($user){
         return $user->canDo('EDIT_USERS',FALSE);
        });


    }
}

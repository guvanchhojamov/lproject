<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','login','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function articles(){
        return $this->hasMany('App\Article');
    }
//    public function comment(){
//        return $this->hasMany('App\Comment');
//    }

    public function roles(){
        return $this->belongsToMany('App\Role','role_user');
    }

    // permission(string) or  permission( array(['View_admin','ADD_ARTICLES'])  )
    public function canDo($permission, $required = FALSE){
        if (is_array($permission)){
            foreach ($permission as $permName){
                $permName = $this->canDo($permName);
                if ($permName && !$required){
                    return TRUE;
                }elseif (!$permName && $required){
                    return FALSE;
                }
            }
            return $required;
        }else{
            foreach ($this->roles as $role){
                foreach ($role->permissions as $perm){
                    if ($permission === $perm->name){
                        return TRUE;
                    }
                }
            }
        }
    }

    public  function hasRole($name,$require=FALSE){
        if (is_array($name)){
            foreach ($name as $nam){
                $nam = $this->hasRole($nam);
                if ($nam && !$require){
                    return TRUE;
                }elseif (!$nam && $require){
                    return FALSE;
                }
            }
            return $require;
        }else{
            foreach ($this->roles as $role){
                foreach ($role as $rol){
                    if ($name === $rol->name){
                        return TRUE;
                    }else{
                        return FALSE;
                    }
                }
            }
        }
        return FALSE;

    }

}

<div class="widget-first widget recent-posts">
    <h3>{{Lang::get('ru.latest_projects_tm')}}</h3>
        <div class="recent-post group">
            @if($comments)
               {{--{{dd($comments)}}--}}
                @foreach($portfolios as $portfolio)
                    <div class="hentry-post group">
                        <div class="thumb-img"><img style="width: 55px;" src="{{asset(env('THEME','pink')).'/assets/images/projects/'.$portfolio->image->mini}}" alt="{{$portfolio->title}}" title="{{$portfolio->title}}" /></div>
                        <div class="text">
                            <a href="{{route('portfolios.show',['alias'=>$portfolio->alias])}}" title="{{strip_tags($portfolio->title)}}" class="title">{!! $portfolio->title !!}</a>
                            {!! substr($portfolio->text,0,100) !!}
                            <a class="read-more" href="{{route('articles.show',['alias'=>$portfolio->alias])}}">
                                &rarr; {{Lang::get('ru.read_more')}}</a>
                        </div>
                    </div>
                @endforeach
            @else
                <h4 style="color:red;text-align: center;">{{Lang::get('ru.not_found_portfolio')}}</h4>
            @endif
        </div>
    </div>

    <div class="widget-last widget recent-comments">
        <h3>{{Lang::get('ru.recent_comments_tm')}}</h3>
        <div class="recent-post recent-comments group">

            @if(!($comments->isEmpty()))
                @foreach($comments as $comment)
                    <div class="the-post group">
                        <div class="avatar">
                        <img alt="" src="{{asset(env('THEME','pink'))}}/assets/images/avatar/{{($comment->user !== null) ? $comment->user->image : 'unknow55.png'}}"
                                             class="avatar" />
                        </div>
                        <span class="author"><strong><a href="#">{{($comment->user !== null) ? $comment->user->name : $comment->name }}</a></strong> in</span>
                        <a class="title" href="{{route('articles.show',['alias'=>$comment->article->alias])}}">
                            {{$comment->article->title}}</a>
                        <p class="comment">
                            {!!substr($comment->text,0,50)!!}..
                            <a class="goto" href="{{route('articles.show',['alias'=>$comment->article->alias])}}">&#187;</a>
                        </p>
                    </div>
                @endforeach
                @else
                <h4 style="color:red;text-align: center;">{{Lang::get('ru.not_found_comments')}}</h4>
            @endif
        </div>
    </div>
<!-- START PAGE META -->
<div id="page-meta">
    <div class="inner group">
        <h3>{{trans('ru.contacts_text_1')}}</h3>
        <h4>{{trans('ru.contacts_text_2')}}</h4>
    </div>
</div>
<!-- END PAGE META -->
<!-- START PRIMARY -->
        <!-- START CONTENT -->
        <div id="content-page" class="content group">
            <div class="hentry group">
                @if(count($errors))
                    <div class="msg-error">
                        <ul style="color:red;">
                        @foreach($errors->all() as $error)
                            <li style="color:red;">{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('status'))
                        <div class="box success-box">
                            {!! session('status') !!}
                        </div>
                @endif
                <form id="contact-form-contact-us" class="contact-form"
                      method="post" action="{{route('contacts')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="usermessagea"></div>
                    <fieldset>
                        <ul>
                            <li class="text-field">
                                <label for="name-contact-us">
                                    <span class="label">Name</span>
                                    <br />					<span class="sublabel">This is the name</span><br />
                                </label>
                                <div class="input-prepend"><span class="add-on"><i class="icon-user"></i></span>
                                    <input type="text" name="name" id="name-contact-us" class="required" value="{{old('name','')}}" />
                                </div>
                                <div class="msg-error"></div>
                            </li>
                            <li class="text-field">
                                <label for="email-contact-us">
                                    <span class="label">Email</span>
                                    <br />					<span class="sublabel">This is a field email</span><br />
                                </label>
                                <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
                                    <input type="text" name="email" id="email-contact-us" class="required email-validate" value="{{old('email')}}" />
                                </div>
                                <div class="msg-error"></div>
                            </li>
                            <li class="textarea-field">
                                <label for="message-contact-us">
                                    <span class="label">Message</span>
                                </label>
                                <div class="input-prepend"><span class="add-on"><i class="icon-pencil"></i></span>
                                    <textarea name="message" id="message-contact-us" rows="8" cols="30" class="required">{{old('message')}}</textarea>
                                </div>
                                <div class="msg-error"></div>
                            </li>
                            <li class="submit-button">
                                {{--<input type="text" name="yit_bot" id="yit_bot" />--}}
                                {{--<input type="hidden" name="yit_action" value="sendmail" id="yit_action" />--}}
                                {{--<input type="hidden" name="yit_referer" value="http://yourinspirationtheme.com/demo/pinkrio/corporate/contact/" />--}}
                                <input type="hidden" name="form_location" value="Pink rio Kontaktlar sahypasyndaky formdan gelen hat!" />
                                <input type="submit" name="yit_sendmail" value="{{trans('ru.send_mail')}}" class="sendmail alignright" />
                            </li>
                        </ul>
                    </fieldset>
                </form>
                {{--<script type="text/javascript">--}}
                    {{--var messages_form_126 = {--}}
                        {{--name: "Please, fill in your name",--}}
                        {{--email: "Please, insert a valid email address",--}}
                        {{--message: "Please, insert your message"--}}
                    {{--};--}}
                {{--</script>--}}
            </div>
            <!-- START COMMENTS -->
            <div id="comments">
            </div>
            <!-- END COMMENTS -->
        </div>
        <!-- END CONTENT -->

        <!-- START SIDEBAR -->
        <!-- END SIDEBAR -->

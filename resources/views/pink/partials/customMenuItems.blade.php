@foreach($items as $item)
    <li {{(URL::current() == $item->url()) ? "class=active":''}}>
        <a href="{{$item->url()}}" style="text-transform: uppercase;">{{$item->title}}</a>
        @if($item->hasChildren())
            <ul class="sub-menu">
                @include(env('THEME','pink').'.partials.customMenuItems',['items'=>$item->children()])
            </ul>
        @endif
    </li>
@endforeach

@extends(env('THEME','pink').'.layouts.site-layout')
@section('header')
    {!! $header !!}
@endsection
@section('content')
    {!! $content !!}
@endsection
@section('footer')
    {!! $footer !!}
@endsection
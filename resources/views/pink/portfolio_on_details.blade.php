@if($portfolio)
    <div id="content-page" class="content group">
        <div class="clear"></div>
        <div class="posts">
            <div class="group portfolio-post internal-post">
                <div id="portfolio" class="portfolio-full-description">

                    <div class="fulldescription_title gallery-filters">
                        <h1>{{$portfolio->title}}</h1>
                    </div>

                    <div class="portfolios hentry work group">
                        <div class="work-thumbnail">
                            <a class="thumb"><img src="{{asset(env('THEME','pink'))}}/assets/images/projects/{{$portfolio->image->path}}" alt="{{strip_tags($portfolio->title)}}" title="{!! $portfolio->title !!}" /></a>
                        </div>
                        <div class="work-description">
                           <div class="t">{!! $portfolio->text !!}</div>
                            <div class="clear"></div>
                            <div class="work-skillsdate">
                                <p class="skills"><span class="label">Filters:</span> {{$portfolio->filter->title}}</p>
                                <p class="workdate"><span class="label">Customer:</span>{!! $portfolio->cutomer !!}</p>
                                @if($portfolio->created_at)
                                    <p class="workdate"><span class="label">Year:</span>{{$portfolio->created_at->format('Y')}}</p>
                                @endif
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>

                    <h3>{{trans('ru.other_projects')}}</h3>

                    <div class="portfolio-full-description-related-projects">
                        @if($portfolios)
                            @foreach($portfolios as $port)
                                <div class="related_project">
                                    <a class="related_proj related_img" href="{{route('portfolios.show',['alias'=>$port->alias])}}" title="{!! $port->title !!}"><img src="{{asset(env('THEME','pink'))}}/assets/images/projects/{{$port->image->mini}}" alt="{{strip_tags($port->title)}}" title="{!! $port->title !!}" /></a>
                                    <h4><a href="{{route('portfolios.show',['alias'=>$port->alias])}}">{!! $port->title !!}</a></h4>
                                </div>
                            @endforeach
                            @else
                            <p style="text-align:center">{{trans('ru.not_other_project')}}</p>
                        @endif

                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    @else
    <p style="text-align:center">Portfolio Tapylmady</p>
@endif

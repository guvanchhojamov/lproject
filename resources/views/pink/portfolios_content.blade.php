<!-- START PAGE META -->
<div id="page-meta">
    <div class="inner group">
        <h3>{{trans('ru.port_text1')}}</h3>
        <h4>{{trans('ru.port_text2')}}</h4>
    </div>
</div>
<!-- END PAGE META -->
@if($portfolios)
    <div id="primary" class="sidebar-no">
        <div class="inner group">
            <!-- START CONTENT -->
            <div id="content-page" class="content group">
                <div class="hentry group">
                    {{--<script>--}}
                        {{--jQuery(document).ready(function($){--}}
                            {{--$('.sidebar').remove();--}}

                            {{--if( !$('#primary').hasClass('sidebar-no') ) {--}}
                                {{--$('#primary').removeClass().addClass('sidebar-no');--}}
                            {{--}--}}

                        {{--});--}}
                    {{--</script>--}}
                    <div id="portfolio" class="portfolio-big-image">
                        @foreach($portfolios as $portfolio)
                            <div class="hentry work group">
                                <div class="work-thumbnail">
                                    <div class="nozoom">
                                        <img src="{{asset(env('THEME','pink'))}}/assets/images/projects/{{$portfolio->image->max}}" alt="{{$portfolio->title}}" title="{{$portfolio->title}}" />
                                        <div class="overlay">
                                            <a class="overlay_img" href="{{asset(env('THEME','pink'))}}/assets/images/projects/{{$portfolio->image->path}}" rel="lightbox" title="Love"></a>
                                            <a class="overlay_project" href="{{route('portfolios.show',['alias'=>$portfolio->alias])}}"></a>
                                            <span class="overlay_title">{!! $portfolio->title !!}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="work-description">
                                    <h3>{!! $portfolio->title !!}</h3>
                                    <div class="t">
                                        {!! substr($portfolio->text,0,250).'...' !!}
                                    </div>
                                    <div class="clear"></div>
                                    <div class="work-skillsdate">
                                        <p class="skills"><span class="label">Filters:</span>{{$portfolio->filter->title }}</p>
                                        <p class="workdate"><span class="label">Customer:</span>{{$portfolio->customer}}</p>
                                        @if($portfolio->created_at)
                                        <p class="workdate"><span class="label">Year:</span>{{$portfolio->created_at->format('Y')}}</p>
                                        @endif
                                    </div>
                                    <a class="read-more" href="{{route('portfolios.show',['alias'=>$portfolio->alias])}}">{{trans('ru.view_project')}}</a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        @endforeach

                            @if($portfolios->lastPage() > 1)
                                <div class="general-pagination group">
                                    @if($portfolios->currentPage() !== 1)
                                        <a href="{{($portfolios->url($portfolios->currentPage()-1)) }}">{!!Lang::get('pagination.previous')!!}</a>
                                    @endif
                                    @for($i=1;$i<=($portfolios->lastpage());$i++)
                                        @if($i == $portfolios->currentPage())
                                            <a  class="selected disabled">{{$i}}</a>
                                        @else
                                            <a href="{{$portfolios->url($i)}}">{{$i}}</a>
                                        @endif
                                    @endfor
                                    @if($portfolios->currentPage() !== $portfolios->lastPage())
                                        <a href="{{($portfolios->url($portfolios->currentPage()+1))}}">{!! Lang::get('pagination.next') !!}</a>
                                    @endif
                                    {{--<a href="#" class="selected">1</a><a href="#">2</a><a href="#">&rsaquo;</a>--}}
                                </div>
                            @endif

                    </div>
                    <div class="clear"></div>
                </div>
                <!-- START COMMENTS -->
                <div id="comments">
                </div>
                <!-- END COMMENTS -->
            </div>
            <!-- END CONTENT -->
            <!-- START EXTRA CONTENT -->
            <!-- END EXTRA CONTENT -->
        </div>
    </div>
    @else
    <p class="" style="text-align: center;">Portfolioda Yok!</p>
@endif

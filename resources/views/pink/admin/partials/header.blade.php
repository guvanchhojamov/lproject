    <div id="header" class="group">

        <div class="group inner">

            <!-- START LOGO -->
            <div id="logo" class="group">
                <a href="/" title="Pink Rio"><img src="{{asset(env('THEME','pink'))}}/assets/images/logo.png" title="Pink Rio" alt="Pink Rio" /></a>
            </div>
            <!-- END LOGO -->
            {{--@foreach($user->roles as $roles)--}}
                {{--{{dd($roles->name)}}--}}
            {{--@endforeach--}}

            <div id="sidebar-header" class="group">
                <div class="widget-first widget yit_text_quote">
                    <blockquote class="text-quote-quote">&#8220;The caterpillar does all the work but the butterfly gets all the publicity.&#8221;</blockquote>
                    <cite class="text-quote-author">Siz:  <b>{{$user->name ?? ''}}</b> - ({{ $user->roles['0']->name ?? '' }})</cite>
                </div>
            </div>
            <div class="clearer"></div>

            <hr />

            <!-- START MAIN NAVIGATION -->
            <div class="menu classic">
                @if($menu)
                    <div class="menu classic" style="text-transform: uppercase;">
                        {!! $menu->asUl(['class'=>'menu']) !!}
                    </div>
                @endif

            </div>
            <!-- END MAIN NAVIGATION -->
            <div id="header-shadow"></div>
            <div id="menu-shadow"></div>
        </div>

    </div>

<div id="content-page" class="content group">
    <div class="hentry group">
        <h3 class="title_page">Menular</h3>

        @if(session('status'))
            <div class="success-box" style="width: 1200px;">
                <ul>
                    <li style="padding: 10px 10px 10px 50px;">{{session()->remove('status')}}</li>
                </ul>
            </div>
        @endif
        <div class="short-table white">
           <table style="width: 1200px;" cellpadding="0" cellspacing="0">
               <thead>
                  <th>Name</th>
                  <th>Link</th>
                  <th>Yok etmek</th>
               </thead>
               @if($menus)
                  @include(env('THEME','pink').'.admin.custom-menu-items',array('items'=>$menus->roots(),'paddingLeft'=>''))
               @endif
           </table>
        </div>
        <a  href="{{route('menus.create')}}" class="btn btn-the-salmon-dance-1" style="color:green;">Taze goshmak+</a>
    </div>
</div>
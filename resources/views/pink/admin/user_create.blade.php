<div id="content-page" class="content group">
    <div class="hentry group" style="width: 1200px;">
        @if(!is_array($errors))
            @if($errors->all())
                <div class="box error-box">
                    <ul style="list-style-type: none;">
                        @foreach($errors->all() as $error)
                            <li style="color: red; font-size: 14px;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        @elseif(is_array($errors))
            <div class="box error-box">
                <ul style="list-style-type: none;">
                    @foreach($errors as $error)
                        <li style="color: red; font-size: 14px;">{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('users.store')}}" class="contact-form" method="POST" enctype="multipart/form-data">
            <ul>
                {{csrf_field()}}
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Ady:</b></span>
                        <br/>
                        <span class="sublabel">User ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="name" value="{{old('name')}}" placeholder="Adyny yazyn...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Login:</b></span>
                        <br/>
                        <span class="sublabel">User login</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="login" value="{{old('login')}}" placeholder="Login..." />
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>E-mail:</b></span>
                        <br/>
                        <span class="sublabel">Email</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="email" placeholder="Email...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Parol:</b></span>
                        <br/>
                        <span class="sublabel">Parol</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="password" name="password" placeholder="password...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Password Confirm:</b></span>
                        <br/>
                        <span class="sublabel">Confirm password</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="password" name="password_confirmation" placeholder="Cofirm password">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Roly:</b></span>
                        <br/>
                        <span class="sublabel">Roly saylan</span><br>
                    </label>
                    <div class="input-prepend">
                        <select name="role_id" id="">
                        @if($roles)
                            @foreach($roles as $k=>$role)
                                    <option value="{{$k+1}}">{{$role->name}}</option>
                            @endforeach
                        @endif
                        </select>
                    </div>
                </li>
                <li class="submit-button">
                    <button class="btn btn-the-salmon-dance-1" name="create_button" style="color: green; font-weight: bold;">Goshmak</button>
                </li>
            </ul>
        </form>

    </div>
</div>
<div class="content group">
    <div class="hentry group">
        <h3 class="title_page">Priveligi</h3>

        @if(session('status'))
            <div class="success-box" style="padding: 10px 0px; width: 1200px;">
                <ul>
                    <li style="margin-left: 50px;">
                        {{session('status')}}
                    </li>
                </ul>
            </div>
        @endif
        {{session_reset()}}

        <form action="{{route('permissions.store')}}" method="POST">
            {{ csrf_field() }}
            <div class="short-table white">
                 <table style="width:1200px;">
                     <th>Priveligiya</th>
                     @if(!$roles->isEmpty())
                         @foreach($roles as $item)
                             <th>{{$item->name}}</th>
                         @endforeach
                     @endif
                     <tbody>
                        @if(!$permissions->isEmpty())
                          @foreach($permissions as $per)
                              <tr>
                                  <td>{{$per->name}}</td>
                                  @foreach($roles as $role)
                                      <td>
                                          @if($role->hasPermission($per->name))
                                              <input type="checkbox" name="{{$role->id}}[]" value="{{$per->id}}" checked="checked"/>
                                              @else
                                              <input type="checkbox" name="{{$role->id}}[]" value="{{$per->id}}"/>
                                          @endif
                                      </td>
                                  @endforeach
                              </tr>
                          @endforeach
                        @endif
                     </tbody>
                 </table>
            </div>
            <input class="btn btn-unplugged-5" type="submit" value="Uytgetmek" name="re_priv_button"/>
        </form>
    </div>
</div>
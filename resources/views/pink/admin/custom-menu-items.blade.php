@if($items)
   @foreach($items as $item)
       <tr>
           <td style="text-align:left">{{$paddingLeft}} <a href="{{route('menus.edit',array('menu'=>$item->id))}}">{{$item->title}}</a></td>
           <td>{{ $item->url() }}</td>

           <td>
             <form action="{{route('menus.destroy',array('menu'=>$item->id))}}" method="POST">
                 {{csrf_field()}}
                 {{method_field('DELETE')}}
                 <button class="btn btn-french-5" type="submit" style="color:red;">Udalit</button>
             </form>
         </td>
     </tr>
       @if($item->hasChildren())
           @include(env('THEME','pink'.'.admin.custom-menu-items'),array('items'=>$item->children(),'paddingLeft'=>$paddingLeft.'--'));
       @endif
   @endforeach
@endif
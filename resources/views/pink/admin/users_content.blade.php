<div id="content-page" class="content group">
    <div class="hentry group">
       <h3 class="title_page">Userler</h3>
        @if(session('status'))
            <div class="box success-box" style="width: 1100px;">
                {{session('status')}}
            </div>
        @endif
        {{session()->remove('status')}}
        <div class="short-table white">
            <table style="width: 1200px;" cellspacing="0" cellpadding="0">
                <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Login</th>
                <th>Role</th>
                <th>Yok Etmek</th>
                </thead>
                @if($users)
                     @foreach($users as $user)
                         <tr>
                             <td style="font-size: 16px !important;">{{$user->id}}</td>
                             <td><a style="font-size: 16px !important;" href="{{route('users.edit',['user'=>$user->id])}}">{{$user->name}}</a></td>
                             <td style="font-size: 16px !important;">{{$user->email}}</td>
                             <td style="font-size: 16px !important;">{{$user->login}}</td>
                             <td style="font-size: 16px !important;">{{$user->roles->implode('name',', ')}}</td>
                             <td style="font-size: 16px !important;">
                                 <form action="{{route('users.destroy',['user'=>$user->id])}}" method="POST">
                                     {{method_field('DELETE')}}
                                     {{csrf_field()}}
                                     <button type="submit" name="del_button" class="btn btn-the-salmon-dance-1" style="color:red; font-weight: bolder;">Delete</button>
                                 </form>
                             </td>
                         </tr>
                     @endforeach
                @endif
            </table>
        </div>
        <a class="btn btn-the-salmon-dance-1" style="color: green; font-weight: bold;" href="{{route('users.create')}}">Taze gohsmak+ </a>
    </div>
</div>

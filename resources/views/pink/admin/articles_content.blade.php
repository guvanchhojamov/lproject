@if($articles)
    <div id="content-page" class="content-group" >
        <div class="hentry group"></div>

        @if(session('status'))
            <div class="box success-box">
            {{session('status')}}
            </div>
        @endif
        {{session()->remove('status')}}
        <h2 style="text-align: center;">Sayta goshulan statyalar</h2>
        <div class="" style="float: right; margin-bottom:1em;">
            <button class="btn" style="padding: 10px 20px;">
                <a href="{{route('aarticles.create')}}" style="color: green;font-weight: bolder;">Taze goshmak+</a>
            </button>
        </div>
            <div class="short-table white">

                <table style="width:100%;" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th class="align-left">ID</th>
                            <th>Bas Sozi</th>
                            <th>Tekst</th>
                            <th>Surat</th>
                            <th>Kategoriya</th>
                            <th>Psevdonim</th>
                            <th>Hereket</th>
                        </tr>
                    </thead>
                    <tbody>
                     @foreach($articles as $article)
                         <tr>
                             <td class="align-left">{{$article->id}}</td>
                             <td class="align-left"><a href="{{route("aarticles.edit",['aarticle'=>$article->alias])}}">
                                     {!! $article->title !!}</a></td>
                             <td class="align-left">{!! substr($article->full_text,0,100) !!}</td>
                             @if(isset($article->image->mini))
                                 <td class="align-left">
                                     <img src="{{asset(env('THEME','pink').'/assets/images/articles/'.$article->image->mini)}}" alt="">
                                 </td>
                                 @else
                                 <td>
                                     <p>No image</p>
                                 </td>
                             @endif
                             <td class="align-left">{!! $article->category->title !!}</td>
                             <td class="align-left">{!! $article->alias !!}</td>
                             <td class="align-left">
                                 <form action="{{route('aarticles.destroy',['aarticle'=>$article->alias])}}"
                                 class="form-horizontal" method="POST">
                                     {{method_field('DELETE')}}
                                     {{csrf_field()}}
                                     <input type="hidden" name="alias" value="{{strip_tags($article->alias)}}">
                                     <button class="btn btn-hem-5" style="background-color:#d86e4d;color:#fff;border:1px solid #d86e4d;" type="submit">Yok etmek</button>
                                 </form>
                             </td>
                         </tr>
                     @endforeach
                    </tbody>
                </table>
            </div>
    </div>

@else
  <div style="text-align:center;">
      <p>Artikller tapylmady</p>
  </div>
@endif
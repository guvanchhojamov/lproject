@if($article)
    <div id="content-page" class="content group">
        <div class="hentry group" style="width: 1200px;">
            @if(!is_array($errors))
                @if($errors->all())
                    <div class="box error-box">
                        <ul style="list-style-type: none;">
                            @foreach($errors->all() as $error)
                                <li style="color: red; font-size: 14px;">{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @elseif(is_array($errors))
                <div class="box error-box">
                    <ul style="list-style-type: none;">
                        @foreach($errors as $error)
                            <li style="color: red; font-size: 14px;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                    <a href="{{route('aarticles.index')}}" class="btn btn-the-salmon-dance-3" style="padding: 10px 15px; color:red;"> <-Yza</a>
            <form action="{{route('aarticles.update',['aarticle'=>$article->alias])}}"
                  class="contact-form" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="update_id" value="{{$article->id}}" />
                <ul>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Ady:</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn ady</span><br>
                        </label>
                        <div class="input-prepend">
                            <input type="text" name="title" value="{{$article->title}}" placeholder="Adyny yazyn...">
                        </div>
                    </li>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Achar sozi(SEO uchin):</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn ady</span><br>
                        </label>
                        <div class="input-prepend">
                            <input type="text" name="keywords" value="{{$article->keywords}}" placeholder="SEO achar sozleri..." />
                        </div>
                    </li>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Meta opisaniya(SEO):</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn ady</span><br>
                        </label>
                        <div class="input-prepend">
                            <input type="text" name="meta_desc" value="{{$article->meta_desc}}" placeholder="Meta mazmuny...">
                        </div>
                    </li>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Psevdonim(Alias):</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn ady</span><br>
                        </label>
                        <div class="input-prepend">
                            <input type="text" name="alias" value="{{$article->alias}}" placeholder="Alias...">
                        </div>
                    </li>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Kichi mazmuny:</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn ady</span><br>
                        </label>
                        <div class="input-prepend">
                            <textarea name="description" id="editor1" cols="30" rows="10">{{ $article->description ?? '' }}</textarea>
                        </div>
                    </li>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Doly text:</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn ady</span><br>
                        </label>
                        <div class="input-prepend">
                            <textarea name="full_text" id="editor2" cols="30" rows="10">{{ $article->full_text ?? ''}}</textarea>
                        </div>
                    </li>

                        @if($article->image->path)
                            <li class="text-field">
                                <label for="name-contact-us">
                                    <span class="label"><b>Statyanyn Suraty:</b></span>
                                    <br/>
                                    {{--<span class="sublabel">Materiyalyn suraty</span><br>--}}
                                </label>
                                <div class="input-prepend">
                                    <img src="{{asset(env('THEME','pink').'/assets/images/articles/'.$article->image->path)}}"
                                         alt="{{strip_tags($article->title)}}">
                                    <input type="hidden" name="old_image" value="{{ $article->image->path }}"/>
                                </div>
                            </li>
                        @endif
                        <li class="text-field">
                            <label for="name-contact-us">
                                <span class="label"><b>Kategoriyasy:</b></span>
                                <br/>
                                <span class="sublabel">Materiyalyn suraty</span><br>
                            </label>
                            <div class="input-prepend">

                                <select name="category_id" id="">
                                    @if($categories)
                                        @foreach($categories as $k=>$category)
                                            <option value="{{$category['id']}}"
                                                    {{ ($category['title'] == $article->category->title)
                                                    ? 'selected' : ''}}>
                                                {{$category['title']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </li>
                    <li class="text-field">
                        <label for="name-contact-us">
                            <span class="label"><b>Taze Surat goymak:</b></span>
                            <br/>
                            <span class="sublabel">Materiyalyn taze suraty</span><br>
                        </label>
                        <div class="input-prepend">
                            <input type="file" name="image" class="filestyle" data-dragdrop="false">
                        </div>
                    </li>
                    @if(isset($article->id))
                        <input type="hidden" name="_method" value="PUT"/>
                    @endif
                    <li class="submit-button">
                        <button class="btn btn-the-salmon-dance-1" name="updae_button" style="padding: 10px 30px;    box-shadow: 7px 10px 11px -8px black;font-weight: bold;
    color: #fff;background-color: forestgreen;border: 1px solid forestgreen;">Uytetmek</button>
                    </li>
                </ul>
            </form>

        </div>
    </div>
@endif
@extends(env('THEME','pink').'.admin.layouts.admin-layout')

@section('header')
    {!! $header !!}
@endsection

@section('footer')
    {!! $footer !!}
@endsection
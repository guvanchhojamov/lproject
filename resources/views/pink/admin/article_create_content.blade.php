
<div id="content-page" class="content group">
    <div class="hentry group" style="width: 1200px;">
        @if(!is_array($errors))
            @if($errors->all())
                <div class="box error-box">
                    <ul style="list-style-type: none;">
                        @foreach($errors->all() as $error)
                            <li style="color: red; font-size: 14px;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @elseif(is_array($errors))
            <div class="box error-box">
                <ul style="list-style-type: none;">
                @foreach($errors as $error)
                    <li style="color: red; font-size: 14px;">{{$error}}</li>
                @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('aarticles.store')}}" class="contact-form" method="POST" enctype="multipart/form-data">
            <ul>
                {{csrf_field()}}
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Ady:</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="title" value="{{old('title')}}" placeholder="Adyny yazyn...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Achar sozi(SEO uchin):</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="keywords" value="{{old('keywords')}}" placeholder="SEO achar sozleri..." />
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Meta opisaniya(SEO):</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="meta_desc" value="{{old('meta_desc')}}" placeholder="Meta mazmuny...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Psevdonim(Alias):</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="alias" value="{{old('alias')}}" placeholder="Alias...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Kichi mazmuny:</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <textarea name="description" id="editor1" cols="30" rows="10">{{(old('description') ?? '')}}</textarea>
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Doly text:</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <textarea name="full_text" id="editor2" cols="30" rows="10">{{(old('full_text') ?? '')}}</textarea>
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Suraty:</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn suraty</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="file" name="image" class="filestyle" data-dragdrop="false">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Kategoriyasy:</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn suraty</span><br>
                    </label>
                    <div class="input-prepend">

                        <select name="category_id" id="">
                        @if($categories)
                            @foreach($categories as $k=>$category)
                                    <optgroup label="{{$k}}">
                                       @foreach($category as $k=>$cat)
                                            <option value="{{$k}}">{{$cat}}</option>
                                        @endforeach

                                    </optgroup>
                            @endforeach
                        @endif
                        </select>
                    </div>
                </li>
                <li class="submit-button">
                    <button class="btn btn-the-salmon-dance-1" name="create_button" style="padding: 10px 30px;    box-shadow: 7px 10px 11px -8px black;font-weight: bold;
    color: #fff;background-color: forestgreen;border: 1px solid forestgreen;">Goshmak</button>
                </li>
            </ul>
        </form>
        
    </div>
</div>
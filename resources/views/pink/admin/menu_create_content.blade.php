<div id="content-page" class="content group">
    <div class="hentry group" style="width: 1200px;">
        @if(!is_array($errors))
            @if($errors->all())
                <div class="box error-box">
                    <ul style="list-style-type: none;">
                        @foreach($errors->all() as $error)
                            <li style="color: red; font-size: 14px;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        @elseif(is_array($errors))

            <div class="box error-box">
                <ul style="list-style-type: none;">
                    @foreach($errors as $error)
                        <li style="color: red; font-size: 14px;">{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{route('menus.store')}}" class="contact-form" method="POST">
            <ul>
                {{csrf_field()}}
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>Ady:</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn ady</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="title" value="{{old('title')}}" placeholder="Adyny yazyn...">
                    </div>
                </li>
                <li class="text-field">
                    <label for="name-contact-us">
                        <span class="label"><b>URL-i</b></span>
                        <br/>
                        <span class="sublabel">Materiyalyn url-i</span><br>
                    </label>
                    <div class="input-prepend">
                        <input type="text" name="url_link" value="" placeholder="url..." />
                    </div>
                </li>
                <li class="submit-button">
                    <button class="btn btn-the-salmon-dance-1" name="create_button" style="padding: 10px 30px;    box-shadow: 7px 10px 11px -8px black;font-weight: bold;
    color: #fff;background-color: forestgreen;border: 1px solid forestgreen;">Goshmak+</button>
                </li>
            </ul>
        </form>
    </div>
</div>
<div id="content-single" class="content group">
    <div class="hentry hentry-post blog-big group ">
        <!-- post featured & title -->
        @if($article)
            <div class="thumbnail">
                <!-- post title -->
                <h1 class="post-title"><a href="#">{!! $article->title !!}</a></h1>
                <!-- post featured -->
                <div class="image-wrap">
                    <img src="{{asset(env('THEME','pink'))}}/assets/images/articles/{{$article->image->max}}" alt="{{$article->title}}" title="{{$article->title}}" />
                </div>
                <p class="date">
                    <span class="month">{{$article->created_at->format('M')}}</span>
                    <span class="day">{{$article->created_at->format('d')}}</span>
                </p>
            </div>
            <!-- post meta -->
            <div class="meta group">
                <p class="author"><span>by <a href="#" title="Posts by {{$article->user->name}}" rel="author">{{$article->user->name}}</a></span></p>
                <p class="categories"><span>In: <a href="{{route('articlesCat',['cat_alias'=>$article->category->alias])}}" title="View all posts in {{$article->category->title}}" rel="category tag">{{$article->category->title}}</a></span></p>
                <p class="comments"><span><a href="#comments" title="Comment on {{$article->title}}">{{count($article->comment)}} {{Lang::choice('ru.comments',count($article->comment))}}</a></span></p>
            </div>
            <!-- post content -->
            <div class="the-content single ">
                {!! $article->full_text!!}
                <div class="socials">
                    <h2>love it, share it!</h2>
                    <a href="https://www.facebook.com/sharer.html?u=http%3A%2F%2Fyourinspirationtheme.com%2Fdemo%2Fpinkrio%2F2012%2F09%2F24%2Fthis-is-the-title-of-the-first-article-enjoy-it%2F&amp;t=This+is+the+title+of+the+first+article.+Enjoy+it." class="socials-small facebook-small" title="Facebook">facebook</a>
                    <a href="https://twitter.com/share?url=http%3A%2F%2Fyourinspirationtheme.com%2Fdemo%2Fpinkrio%2F2012%2F09%2F24%2Fthis-is-the-title-of-the-first-article-enjoy-it%2F&amp;text=This+is+the+title+of+the+first+article.+Enjoy+it." class="socials-small twitter-small" title="Twitter">twitter</a>
                    <a href="https://plusone.google.cgroupom/_/+1/confirm?hl=en&amp;url=http%3A%2F%2Fyourinspirationtheme.com%2Fdemo%2Fpinkrio%2F2012%2F09%2F24%2Fthis-is-the-title-of-the-first-article-enjoy-it%2F&amp;title=This+is+the+title+of+the+first+article.+Enjoy+it." class="socials-small google-small" title="Google">google</a>
                    <a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fyourinspirationtheme.com%2Fdemo%2Fpinkrio%2F2012%2F09%2F24%2Fthis-is-the-title-of-the-first-article-enjoy-it%2F&amp;media=http://yourinspirationtheme.com/demo/pinkrio/files/2012/09/00212.jpg&amp;description=Fusce+nec+accumsan+eros.+Aenean+ac+orci+a+magna+vestibulum+posuere+quis+nec+nisi.+Maecenas+rutrum+vehicula+condimentum.+Donec+volutpat+nisl+ac+mauris+consectetur+gravida.+Lorem+ipsum+dolor+sit+amet%2C+consectetur+adipiscing+elit.+Donec+vel+vulputate+nibh.+Pellentesque%5B...%5D" class="socials-small pinterest-small" title="Pinterest">pinterest</a>
                    <a href="http://yourinspirationtheme.com/demo/pinkrio/2012/09/24/this-is-the-title-of-the-first-article-enjoy-it/" class="socials-small bookmark-small" title="This is the title of the first article. Enjoy it.">bookmark</a>
                </div>
            </div>
            <p class="tags">Tags: <a href="#" rel="tag">book</a>, <a href="#" rel="tag">css</a>, <a href="#" rel="tag">design</a>, <a href="#" rel="tag">inspiration</a></p>
            <div class="clear"></div>
            @else
            <h2 style="color:red;text-align:center;">Article tapylmady!</h2>
        @endif
    </div>
    <!-- START COMMENTS -->
    <div id="comments">
        <h3 id="comments-title">
            <span>{{count($article->comments)}}</span> {{Lang::choice('ru.comments',count($article->comments))}}
        </h3>
        @set ($com,$article->comments->groupBy('parent_id'))
        @if(count($article->comments) > 0)

        @endif

        <ol class="commentlist group">
            @foreach($com as $k=>$comments)
                @if($k !==0)
                    @break
                @endif
                @include(env('THEME','pink').'.comment',['items'=>$comments])
            @endforeach
        </ol>

        <!-- START TRACKBACK & PINGBACK -->
        <h2 id="trackbacks">Trackbacks and pingbacks</h2>
        <ol class="trackbacklist"></ol>
        <p><em>No trackback or pingback available for this article.</em></p>

        <!-- END TRACKBACK & PINGBACK -->
        <div id="respond">
            <div class="wrap_result bg-info" style="padding: 15px 10px;text-align: center;z-index:6000; display: none;color:green;height:auto;border:1px solid green;font-size: 16px;border-radius:5px;background-color: #fff;"></div>
            <h3 id="reply-title">Leave a <span>Reply</span> <small><a rel="nofollow" id="cancel-comment-reply-link" href="#respond" style="display:none;">Cancel reply</a></small></h3>

            <form action="{{route('comment.store')}}" method="post" id="commentform">
                {{csrf_field()}}
                @if(!Auth::check())
                <p class="comment-form-author"><label for="author">Name</label> <input id="author" name="name" type="text" value="" size="30" aria-required="true" /></p>
                <p class="comment-form-email"><label for="email">Email</label> <input id="email" name="email" type="text" value="" size="30" aria-required="true" /></p>
                <p class="comment-form-url"><label for="url">Website</label><input id="url" name="site" type="text" value="" size="30" /></p>
                 @else
                    <div class="light_rounded">
                        <p>Siz kommentariyany -  <strong>{{($article->user !== 0) ? $article->user->name : 'Anonymous'}}</strong> hokmunde goshyanyz!</p>
                    </div>
                @endif

                <p class="comment-form-comment"><label for="comment">Your comment</label><textarea id="comment" name="text" cols="45" rows="8"></textarea></p>
                <div class="clear"></div>
                <p class="form-submit">
                    <input type="hidden" id="comment_post_ID" name="comment_post_ID" value="{{$article->id}}">
                    <input type="hidden" id="comment_parent" name="comment_parent" value="0">
                    <input name="submit" type="submit" id="submit" value="Post Comment" />
                </p>
            </form>

        </div>
        <!-- #respond -->
    </div>
    <!-- END COMMENTS -->
</div>
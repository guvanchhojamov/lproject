@extends(env('THEME','pink').'.layouts.site-layout')
@section('header')
    {!! $header !!}
@endsection

@section('content')
    <div id="content-page" class="content group">
        <div class="hentry group">
            @if(count($errors))
            <div class="error-box" style="text-align: center;">
                    @foreach($errors->all() as $error)
                        <ul>
                            <li style="color:red;">{{$error}}</li>
                        </ul>
                    @endforeach
            </div>
            @endif
            <form id="contact-form-contact-us" class="contact-form" method="post" action="{{url('/login')}}" enctype="multipart/form-data">
                <div class="usermessagea">
                </div>
                <fieldset>
                    {{csrf_field()}}
                    <ul style="margin-top: 1em !important;">
                        <li class="text-field" style="float: none;margin:0 auto;">
                            <label for="name-contact-us">
                                <span class="label" style="text-transform: capitalize">{{config('SettingsCustom.auth_field_for_login')}}</span>
                            </label>
                            <div class="input-prepend"><span class="add-on"><i class="icon-user"></i></span><input type="text" name="{{config('SettingsCustom.auth_field_for_login')}}" id="name-contact-us" class="required" value="" /></div>
                            <div class="msg-error"></div>
                        </li>
                        <li class="text-field" style="float: none;margin:0 auto;">
                            <label for="email-contact-us">
                                <span class="label">Password</span>
                            </label>
                            <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span><input type="password" name="password" id="email-contact-us" class="required email-validate" value="" /></div>
                            <div class="msg-error"></div>
                        </li>
                        <li class="submit-button" style="margin-right:26%;">
                            <input type="submit" name="login_buotton" value="{{trans('ru.login')}}" class="sendmail alignright" />
                        </li>
                    </ul>
                </fieldset>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <div style="margin-top: 12em;">
    {!! $footer !!}
    </div>
@endsection
